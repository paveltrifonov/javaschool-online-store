<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>

</head>
<body>
<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>

<h1>Create Subcategory</h1>
<div class="container">

<form:form modelAttribute="categoryInfo" method="POST">
        <div class="row">
        <div class=" col-sm-6 col-lg-6">
            <label for="typeName">Category</label>
            <form:select class="form-control" name="select" path="category" >
                <c:forEach var="categoriesValue" items="${categoryList}">
                    <form:option value="${categoriesValue}">${categoriesValue} </form:option>
                </c:forEach>
            </form:select>
        </div>
        </div>
        <br>
        <div class="row">
            <div class=" col-sm-6 col-lg-6">
                 <div class="form-group">
                    <label for="typeName">Subcategory Name</label>
                        <form:input id="typeName" path="typeName" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
                         <form:errors path="typeName" class="error-message" />
                </div>
            </div>
         </div>

    <div class="row">
        <div class=" col-sm-6 col-lg-6">
            <label for="typeName">Parameters</label>
            <form:select class="form-control" name="select[]" path="attributes" multiple="true" required="true">
                    <c:forEach var="attributeValue" items="${attributeList}">
                        <form:option value="${attributeValue}">${attributeValue} </form:option>
                    </c:forEach>
            </form:select>
        </div>
    </div>
    <br>
    <div class=" col-sm-6 col-lg-6">
        <input type="submit" value="Submit" class="btn btn-primary" />
    </div>
    <br>
</form:form>
    <br>
    <h3>Subcategory list</h3>
    <div class=" col-sm-6 col-lg-6">
        <ul class="list-group">
            <c:forEach items="${subcategoryList}" var="cat">
                <li class="list-group-item">${cat}</li>
            </c:forEach>
        </ul>
    </div>
</div>
</body>
</html>