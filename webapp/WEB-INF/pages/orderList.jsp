<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Order List</title>


</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>Order List<h1>

    <h3>Total Order Count: ${paginationResult.totalRecords}</h3>

<table class="table table-bordered">
    <thead>
    <tr class="active">
        <th>Order Num</th>
        <th>Order Date</th>
        <th>Customer Name</th>
        <th>Customer Address</th>
        <th>Customer Email</th>
        <th>Amount</th>
        <th>Status</th>
        <th>View</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${paginationResult.list}" var="orderInfo">
        <tr class="active">
            <td>${orderInfo.orderNum}</td>
            <td>
                <fmt:formatDate value="${orderInfo.orderDate}" pattern="dd-MM-yyyy HH:mm"/>
            </td>
            <td>${orderInfo.customerName}</td>
            <td>${orderInfo.customerAddress}</td>
            <td>${orderInfo.customerEmail}</td>
            <td style="color:red;">
                <fmt:formatNumber value="${orderInfo.amount}" type="currency"/>
            </td>
            <td><a href="${pageContext.request.contextPath}/changeStatus?orderId=${orderInfo.id}">
                    ${orderInfo.status}</a></td>
            <td><a href="${pageContext.request.contextPath}/order?orderId=${orderInfo.id}">
                View</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<br>
<c:if test="${paginationResult.totalPages > 1}">
    <div class="container">
        <c:forEach items="${paginationResult.navigationPages}" var = "page">
            <c:if test="${page != -1 }">
                <a href="orderList?page=${page}" class="nav-item">${page}</a>
            </c:if>
            <c:if test="${page == -1 }">
                <span class="nav-item"> ... </span>
            </c:if>
        </c:forEach>

    </div>
</c:if>

</body>
</html>