<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>

</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<h1>Create Product</h1>
<div class="container">
    <form:form modelAttribute="productForm"  method="POST">

        <h3>Category</h3>
        <form:select class="form-control" name="select" path="category" >
            <c:forEach var="categoriesValue" items="${categoriesList}">
                <form:option value="${categoriesValue}">${categoriesValue} </form:option>
            </c:forEach>
        </form:select>
        <br>
        <table >
            <td><input type="submit" value="Submit" class="btn btn-primary" /> </td>
            </td>
            </tr>
        </table>

    </form:form>
</div>

</body>
</html>