<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>

</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<h1>Category parameter</h1>
<div class="container">
<form:form modelAttribute="attributesForm" method="POST">
    <div class="form-group">
        <label for="attributeName">Create Parameter</label>
        <form:input  id="attributeName" path="attributeName"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
        <form:errors path="attributeName" class="error-message" />
    </div>
    <br>
    <table >
        <td><input type="submit" value="Submit" class="btn btn-primary" /> </td>
        </tr>
    </table>
</form:form>
    <h3>Parameter list</h3>
    <div class="col-md-4">
        <ul class="list-group">
            <c:forEach items="${attributeList}" var="atr">
             <li class="list-group-item">${atr}</li>
            </c:forEach>
        </ul>
    </div>
</div>
</body>
</html>