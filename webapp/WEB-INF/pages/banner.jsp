<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Banner administration</title>


</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>Banner administration</h1>

<div class="container-fluid">
    <form:form method="POST" modelAttribute="bannerForm" action="${pageContext.request.contextPath}/banner">
        <div class="row">
            <input class="btn btn-primary" type="submit" value="Update" />
        </div>
        <br>
        <div class="row equal">
        <c:forEach items="${paginationProducts.list}" var="prodInfo" varStatus="varStatus">
            <form:hidden path="bannerInfos[${varStatus.index}].code" value="${prodInfo.code}"/>
        <div class=" col-sm-2 col-lg-2">
            <ul class="list-group">
                <li class="list-group-item"><img src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" class="rounded" alt="Cinque Terre" style="width:100%"></li>
                <li class="list-group-item">${prodInfo.name}</li>
                <li class="list-group-item">Category: ${prodInfo.productType}</li>
                <c:forEach items="${attributes}" var="parameters">
                    <c:if test="${prodInfo.code==parameters.code}">
                        <li class="list-group-item">${parameters.category}: ${parameters.parameter}</li>
                    </c:if>
                </c:forEach>
                <li class="list-group-item">Price: <fmt:formatNumber value="${prodInfo.price}" type="currency"/></li>
                <c:if test="${prodInfo.banner==true}">
                <li class="list-group-item"><label class="checkbox-inline"><form:checkbox path="bannerInfos[${varStatus.index}].banner" value="true" checked="true" />On banner</label></li>
                </c:if>
                <c:if test="${prodInfo.banner==false}">
                    <li class="list-group-item"><label class="checkbox-inline"><form:checkbox path="bannerInfos[${varStatus.index}].banner" value="true" />On banner</label></li>
                </c:if>
                <%--<c:if test="${prodInfo.banner==false}">--%>
                    <%--<li class="list-group-item"><label class="checkbox-inline"><form:input path="bannerInfos[${varStatus.index}].banner"  type="checkbox" value="false"/>On banner</label></li>--%>
                <%--</c:if>--%>
            </ul>
        </div>

        </c:forEach>
        </div>
    <br/>
    </form:form>
</div>
<div class="container">
        <div class="col-lg-12">
            <c:if test="${paginationProducts.totalPages > 1}">
                <ul class="pagination">
                    <c:forEach items="${paginationProducts.navigationPages}" var = "page">
                        <c:if test="${page != -1 }">
                            <li><a href="banner?page=${page}" class="nav-item">${page}</a></li>
                        </c:if>
                     <c:if test="${page == -1 }">
                         <span class="nav-item"> ... </span>
                     </c:if>
                    </c:forEach>
                </ul>
            </c:if>
        </div>
</div>

</body>
</html>