<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Registration success</title>

</head>
<body>
<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>

<div class="container">
    <div class="center-block">
    <div class="alert alert-success">
        <strong>Parameter was successfully created</strong>
    </div>
</div>
</div>

</body>
</html>