<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Account Info</title>


</head>
<body>


<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
<jsp:include page="_menu.jsp" />
</security:authorize>
<h1>Account Info</h1>
<div class="row">
<div class="col-md-2">
    <ul class="list-group">
        <li class="list-group-item">User Name: ${account.userName}</li>
        <li class="list-group-item">User Role:${account.userRole}</li>
        <li class="list-group-item">Name:${account.name}</li>
        <li class="list-group-item">Address:${account.address}</li>
        <li class="list-group-item">Email:${account.email}</li>
        <li class="list-group-item">Phone:${account.phone}</li>
    </ul>
</div>
</div>
<br>
<div class="row">
    <button type="button" class="btn btn-primary" onClick="location.href='${pageContext.request.contextPath}/changePassword'">Change Password</button>
    <button type="button" class="btn btn-primary" onClick="location.href='${pageContext.request.contextPath}/changeAccountInfo'">Change Account Information</button>
</div>
</body>
</html>