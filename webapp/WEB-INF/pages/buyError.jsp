<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Buy Error</title>


</head>
<body>
<jsp:include page="_header.jsp" />

<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>

<div class="container">
    <div class="alert alert-danger">
        <div class="text-center">
            <h3>Sorry, there is not the right amount of goods</h3>
        </div>
    </div>
</div>

</body>
</html>