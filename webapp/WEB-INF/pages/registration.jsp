
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Registration</title>


</head>
<body>
<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize><div class="container">

        <h1>Registration</h1>

<form:form method="POST" modelAttribute="accountInfo" action="${pageContext.request.contextPath}/registration">

    <div class="form-group">
        <label for="userName">User Name:</label>
        <form:input id="userName" path="userName" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="userName" class="error-message" />
    </div>

    <div class="form-group">
        <label for="pwd">Password:</label>
        <form:input id="pwd" path="password" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="password" class="error-message" />
    </div>

    <div class="form-group">
        <label for="mpwd">Confirm Password:</label>
        <form:input id="mpwd" path="matchingPassword" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="matchingPassword" class="error-message" />
    </div>

    <div class="form-group">
        <label for="name">Full name:</label>
        <form:input id="name" path="name" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="name" class="error-message" />
    </div>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var suggestView1 = new ymaps.SuggestView('suggest1');
        }
    </script>

    <div class="form-group">
        <label for="suggest1">Address:</label>
        <form:input path="address" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="suggest1"/>
        <form:errors path="address" class="error-message" />
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <form:input id="email" path="email" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="email" class="error-message" />
    </div>

    <div class="form-group">
        <label for="phone">Phone:</label>
        <form:input path="phone" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="phone" class="error-message" />
    </div>

    <table >

        <td>&nbsp;</td>
            <td><input type="submit" value="Submit" class="btn btn-primary" /></td>
        </tr>
    </table>

</form:form>



</body>
</html>

