<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Change password</title>

</head>
<body>


<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<div class="container">
<h1>Change password</h1>
<form:form method="POST" modelAttribute="changePasswordInfo" action="${pageContext.request.contextPath}/changePassword">

    <div class="form-group">
        <label for="pwd">Old password:</label>
        <form:input id="pwd" path="oldPassword" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="oldPassword" class="error-message" />
    </div>
    <div class="form-group">
        <label for="npwd">New password:</label>
        <form:input id="npwd" path="newPassword" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="newPassword" class="error-message" />
    </div>
    <div class="form-group">
        <label for="cpwd">Confirm new password:</label>
        <form:input id="cpwd" path="matchingPassword" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="matchingPassword" class="error-message" />
    </div>
    <table >

        <td>&nbsp;</td>
        <td><input type="submit" value="Submit" class="btn btn-primary" /> <input type="reset"
                                                                                  value="Reset" class="btn btn-light" /></td>
        </tr>
    </table>


</form:form>
</div>
</body>
</html>