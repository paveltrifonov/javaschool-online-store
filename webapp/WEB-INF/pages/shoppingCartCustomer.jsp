<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Enter Customer Information</title>

</head>
<body>
<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize><div class="container">
<h1>Enter Customer Information</h1>

<form:form method="POST" modelAttribute="customerForm" action="${pageContext.request.contextPath}/shoppingCartCustomer">

    <div class="form-group">
        <label for="name">Full Name:</label>
        <form:input id="name" path="name" value="${account.name}" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="name" class="error-message" />
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <form:input id="email" path="email" value="${account.email}" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="email" class="error-message" />
    </div>
    <div class="form-group">
        <label for="phone">Phone:</label>
        <form:input id="phone" path="phone" value="${account.phone}" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="phone" class="error-message" />
    </div>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var suggestView1 = new ymaps.SuggestView('suggest1');
        }
    </script>

    <div class="form-group">
        <label for="suggest1">Address:</label>
        <form:input id="suggest1" path="address" value="${account.address}" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="address" class="error-message" />
    </div>
    <table>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="Submit" class="btn btn-primary" /></td>
        </tr>
    </table>

</form:form>

</body>
</html>