<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Shopping Cart</title>

</head>
<body>
<jsp:include page="_header.jsp" />

<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>My Cart</h1>
<div class="container-fluid">
<c:if test="${empty cartForm or empty cartForm.cartLines}">
    <h2>There are no items in Cart</h2>
    <button type="button" class="btn btn-light" onClick="location.href='${pageContext.request.contextPath}/index'">Home</button>
</c:if>

<c:if test="${not empty cartForm and not empty cartForm.cartLines   }">
    <form:form method="POST" modelAttribute="cartForm" action="${pageContext.request.contextPath}/shoppingCart">
        <c:forEach items="${cartForm.cartLines}" var="cartLineInfo" varStatus="varStatus">


                <div class=" col-sm-2 col-lg-2">
                    <ul class="list-group" >
                        <li class="list-group-item"><img src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" class="rounded" alt="Cinque Terre" style="width:100%"></li>
                        <form:hidden path="cartLines[${varStatus.index}].productInfo.code" />
                        <li class="list-group-item">${cartLineInfo.productInfo.name}</li>
                        <li class="list-group-item">Price: <span class="text-primary"><fmt:formatNumber value="${cartLineInfo.productInfo.price}" type="currency"/></span></li>
                        <li class="list-group-item">Storage: <fmt:formatNumber value="${cartLineInfo.productInfo.storage}" type="number"/></li>
                        <li class="list-group-item">Quantity:<form:input type="number" path="cartLines[${varStatus.index}].quantity" min="1" max="${cartLineInfo.productInfo.storage}" required="true"/> <input class="btn btn-primary" type="submit" value="Update Quantity" /></li>
                        <li class="list-group-item">Subtotal:<span class="text-danger"><fmt:formatNumber value="${cartLineInfo.amount}" type="currency"/></span></li>
                        <li class="list-group-item"><a href="${pageContext.request.contextPath}/shoppingCartRemoveProduct?code=${cartLineInfo.productInfo.code}"><p class="text-danger">Delete</p></a></li>
                    </ul>
            </div>
        </c:forEach>
        <div style="clear: both"></div>


        <security:authorize  access="hasAnyRole('ROLE_USER')">
            <button type="button" class="btn btn-light" onClick="location.href='${pageContext.request.contextPath}/shoppingCartCustomer'">Enter Customer Info</button>
        </security:authorize>

        <security:authorize  access="isAnonymous()">
            <button type="button" class="btn btn-light" onClick="location.href='${pageContext.request.contextPath}/login'">Login to enter Customer Info</button>
        </security:authorize>
        <button type="button" class="btn btn-light" onClick="history.back()">Continue Buy</button>
    </form:form>


</c:if>


</div>

</body>
</html>