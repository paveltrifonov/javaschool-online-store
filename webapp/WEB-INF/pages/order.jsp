<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Order Info</title>
</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h3>Order Info</h3>

<div class="row">
    <h3>Customer Information:</h3>
    <div class="col-md-2">
        <ul class="list-group">
            <li class="list-group-item">Name: ${orderInfo.customerName}</li>
            <li class="list-group-item">Email: ${orderInfo.customerEmail}</li>
            <li class="list-group-item">Phone: ${orderInfo.customerPhone}</li>
            <li class="list-group-item">Address: ${orderInfo.customerAddress}</li>
        </ul>
        <h3>Order Summary:</h3>
        <ul class="list-group">
            <li class="list-group-item">Total:<span class="text-danger"><fmt:formatNumber value="${orderInfo.amount}" type="currency"/></span></li>
        </ul>
    </div>
</div>



<br/>

<table class="table table-bordered">\
    <thead>
    <tr class="active">
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${orderInfo.details}" var="orderDetailInfo">
        <tr class="active">
            <td>${orderDetailInfo.productCode}</td>
            <td>${orderDetailInfo.productName}</td>
            <td>${orderDetailInfo.quanity}</td>
            <td>
                <fmt:formatNumber value="${orderDetailInfo.price}" type="currency"/>
            </td>
            <td>
                <fmt:formatNumber value="${orderDetailInfo.amount}" type="currency"/>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<c:if test="${paginationResult.totalPages > 1}">
    <div class="container">
        <c:forEach items="${paginationResult.navigationPages}" var = "page">
            <c:if test="${page != -1 }">
                <a href="orderList?page=${page}" class="nav-item">${page}</a>
            </c:if>
            <c:if test="${page == -1 }">
                <span class="nav-item"> ... </span>
            </c:if>
        </c:forEach>

    </div>
</c:if>




<jsp:include page="_footer.jsp" />

</body>
</html>