<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<div class="container-custom">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="carousel/3006f893__1_.KXmun.jpg" alt="Los Angeles" style="width:100%;">
                <div class="carousel-caption">
                    <h2>Clothes for every taste</h2>
                </div>
            </div>

            <div class="item">
                <img src="carousel/christmas-gift-new-year-hd-wallpaper.uWtwW.jpg" alt="Chicago" style="width:100%;">
                <div class="carousel-caption">
                    <h2>From 100$ free delivery</h2>
                </div>
            </div>

            <div class="item">
                <img src="carousel/odejda.uJMOv.jpg" alt="New York" style="width:100%;">
                <div class="carousel-caption">
                    <h2>Wide selection of clothes</h2>
                </div>
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

</body>
</html>
