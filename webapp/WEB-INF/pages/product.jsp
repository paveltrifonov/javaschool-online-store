<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>

</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<h3>Create Product</h3>
    <div class="container">
    <form:form modelAttribute="productForm" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="code">Code</label>
            <form:input  id="code" path="code"  type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"  required="true"/>
            <form:errors path="code" class="error-message" />
        </div>
        <div class="form-group">
            <label for="name">Name</label>
            <form:input  id="name" path="name"  type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
            <form:errors path="name" class="error-message" />
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <form:input  id="price" path="price"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
            <form:errors path="price" class="error-message" />
        </div>
        <div class="form-group">
            <label for="storage">Storage</label>
            <form:input  id="storage" path="storage"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"  min="0" required="true"/>
            <form:errors path="storage" class="error-message" />
        </div>
        <div class="form-group">
            <label for="productType">Category</label>
            <form:input disabled="true" id="productType" path="productType" value="${productType}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
            <form:errors path="productType" class="error-message" />
        </div>
        <form:input hidden="true" path="productType" value="${productType}" />


        <c:forEach var="element" items="${attributeList}">
            <div class="form-group">
                <label for="atrValue">${element}</label>
                <form:input id="atrValue" path="atrValue"  type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
            </div>
        </c:forEach>

            Upload Image
            <form:input type="file" path="fileData"/></td>

        <br>
        <table >
            <td><input type="submit" value="Submit" class="btn btn-primary" /> </td>
            </tr>
        </table>
    </form:form>

    </div>

</body>
</html>