<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Shopping Cart Confirmation</title>



</head>
<body>
<jsp:include page="_header.jsp" />

<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>Confirmation</h1>



<div class="row">
    <h3>Customer Information:</h3>
    <div class="col-md-2">
    <ul class="list-group">
        <li class="list-group-item">Name: ${myCart.customerInfo.name}</li>
        <li class="list-group-item">Email: ${myCart.customerInfo.email}</li>
        <li class="list-group-item">Phone: ${myCart.customerInfo.phone}</li>
        <li class="list-group-item">Address: ${myCart.customerInfo.address}</li>
    </ul>
    <h3>Cart Summary:</h3>
        <ul class="list-group">
        <li class="list-group-item">Quantity: ${myCart.quantityTotal}</li>
        <li class="list-group-item">Total:<span class="text-danger"><fmt:formatNumber value="${myCart.amountTotal}" type="currency"/></span></li>
    </ul>
    </div>
</div>

<form method="POST" action="${pageContext.request.contextPath}/shoppingCartConfirmation">
    <div class="container">
    <!-- Edit Cart -->
        <button type="button" class="btn btn-light" onClick="location.href='${pageContext.request.contextPath}/shoppingCart'">Edit Cart</button>
    <!-- Edit Customer Info -->
        <button type="button" class="btn btn-light" onClick="location.href='${pageContext.request.contextPath}/shoppingCartCustomer'">Edit Customer Info</button>
    <!-- Send/Save -->
    <input type="submit" value="Send" class="btn btn-success" />
    </div>
</form>
<br>
<div class="container-fluid">

    <c:forEach items="${myCart.cartLines}" var="cartLineInfo">


        <div class=" col-sm-2 col-lg-2">
            <ul class="list-group" >
                <li class="list-group-item"><img src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" class="rounded" alt="Cinque Terre" style="width:100%"></li>
                <form:hidden path="cartLines[${varStatus.index}].productInfo.code" />
                <li class="list-group-item">${cartLineInfo.productInfo.name}</li>
                <li class="list-group-item">Price: <span class="text-primary"><fmt:formatNumber value="${cartLineInfo.productInfo.price}" type="currency"/></span></li>
                <li class="list-group-item">Quantity: ${cartLineInfo.quantity}</li>
                <li class="list-group-item">Subtotal:<span class="text-danger"><fmt:formatNumber value="${cartLineInfo.amount}" type="currency"/></span></li>
            </ul>
        </div>

    </c:forEach>

</div>

<jsp:include page="_footer.jsp" />

</body>
</html>