<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Change order status</title>

</head>
<body>


<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>

<h1>Change order status</h1>
<form:form method="POST" modelAttribute="orderInfo" action="${pageContext.request.contextPath}/changeStatus">
    <c:if test="${orderInfo.status == 'Paid'}">
    <p><select path="status" required name="status">
        <option value="Sent">Sent</option>
        <option value="Received">Received</option>
    </select></p>

    <form:input hidden="true" path="id" value="${orderId}" />
    <input type="submit" value="Submit" /><input type="button" onclick="history.back();" value="Back"/>
        </c:if>

        <c:if test="${orderInfo.status == 'Sent'}">
        <p><select path="status" required name="status">
            <option value="Received">Received</option>
        </select></p>

            <form:input hidden="true" path="id" value="${orderId}" />
        <input type="submit" value="Submit" /><input type="button" onclick="history.back();" value="Back"/>
         </c:if>

    <c:if test="${orderInfo.status == 'Received'}">
        Order already received.
        <br><input type="button" onclick="history.back();" value="Back"/></br>
    </c:if>
</form:form>


<jsp:include page="_footer.jsp" />

</body>
</html>