
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product List</title>


</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>${filterProduct.category}</h1>
<div class="container-fluid">

    <div class="row">
        <div class=" col-sm-1 col-lg-1">
            <form:form modelAttribute="filterProduct" method="post">
            <form:hidden path="category" value="${filterProduct.category}"/>
            <label >Price</label>
            <form:select class="form-control" name="select" path="name" multiple="false">
                <option value="" selected>-</option>
                <option value="30">Up to 30$</option>
                <option value="50">Up to 50$</option>
                <option value="100">Up to 100$</option>
                <option value="200">Up to 200$</option>
                <option value="300">Up to 300$</option>
                <option value="500">Up to 500$</option>
            </form:select>
        </div>

        <c:forEach var="atr" items="${filterAttrValues}">
            <div class=" col-sm-1 col-lg-1">
                <label >${atr.attributeName}</label>
                <form:select class="form-control" name="select" path="atrValue" multiple="false">
                    <option value="" selected>-</option>
                    <c:forEach var="values" items="${atr.attributeValues}" varStatus="status">
                        <c:if test="${values.product.productType==filterProduct.category && values.product.storage!=0}">
                            <form:option value="${values.attributeValueName}">${values.attributeValueName} </form:option>
                        </c:if>
                    </c:forEach>
                </form:select>
            </div>
        </c:forEach>

        <input type="submit" value="Filter" class="btn btn-primary" />
        </form:form>
    </div>
    <br>
    <div class="row equal">
    <c:set var="value" value="true" />
<c:forEach items="${trueProducts}" var="prodInfo">
    <c:if test="${prodInfo.productType==filterProduct.category && (prodInfo.price <=filterPrice || filterPrice==0) && prodInfo.storage!=0}" >
        <c:set var="value" value="false" />
        <div class=" col-sm-2 col-lg-2">
            <ul class="list-group">
                <li class="list-group-item"><img src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" class="rounded" alt="Cinque Terre" style="width:100%"></li>
                <li class="list-group-item">${prodInfo.name}</li>
                <li class="list-group-item">Category: ${prodInfo.productType}</li>
                <c:forEach items="${attributes}" var="parameters">
                    <c:if test="${prodInfo.code==parameters.code}">
                        <li class="list-group-item">${parameters.category}: ${parameters.parameter}</li>
                    </c:if>
                </c:forEach>
                <li class="list-group-item">Price: <fmt:formatNumber value="${prodInfo.price}" type="currency"/></li>
                <security:authorize access="hasAnyRole('ROLE_USER','ROLE_ANONYMOUS')">
                    <li class="list-group-item"><a href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}">Buy Now</a></li>
                </security:authorize>
                <!-- For Manager edit Product -->
                <security:authorize  access="hasRole('ROLE_MANAGER')">
                    <li class="list-group-item"><a href="${pageContext.request.contextPath}/editProduct?code=${prodInfo.code}">Edit Product</a></li>
                </security:authorize>
            </ul>
        </div>
    </c:if>
</c:forEach>
    </div>
<c:if test="${value==true}">
    <h2>There are no such goods</h2>
</c:if>


</div>
</body>
</html>