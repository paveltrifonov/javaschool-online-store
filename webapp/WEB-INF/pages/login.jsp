<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Login</title>


</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<div class="container">

    <h1>Login</h1>

    <!-- /login?error=true -->
    <c:if test="${param.error == 'true'}">
        <div style="color: red; margin: 10px 0px;">
            Login Failed!!!<br /> Reason :
                ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
        </div>
    </c:if>

    <form method="POST"
          action="${pageContext.request.contextPath}/j_spring_security_check">

        <div class="form-group">
            <label for="userName">User Name:</label>
            <input class="form-control" id="userName" name="userName">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="password">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

    <span class="error-message">${error }</span>

</div>

</body>
</html>