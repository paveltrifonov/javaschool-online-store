<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<div class="menu-container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/productList">Product List</a></li>
                <security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/orderList">Order List</a>
                </security:authorize>
                 <security:authorize  access="hasRole('ROLE_MANAGER')">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/productCategory">Create Product</a></li>
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/category">Create Category</a></li>
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/attribute">Create Parameter</a></li>
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/banner">Banner administration</a></li>
                 </security:authorize>
        </ol>
    </nav>

</div>