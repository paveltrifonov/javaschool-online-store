<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Access Denied</title>

</head>
<body>


<jsp:include page="_header.jsp" />
<jsp:include page="_menu.jsp" />

<h1>Access Denied!</h1>

<div class="container">
<div class="alert alert-danger">
    <strong>Sorry, you can not access this page!</strong>
</div>
</div>

</body>
</html>