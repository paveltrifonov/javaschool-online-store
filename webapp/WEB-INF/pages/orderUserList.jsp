<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Order List</title>

</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<fmt:setLocale value="en_US" scope="session"/>

<h1>Order List</h1>

<c:set var="value" value="true" />
<c:forEach items="${orders}" var="orderInfo" >
    <c:set var="value" value="false" />
</c:forEach>
    <c:if test="${value==false}">
        <table class="table table-bordered">
            <thead>
            <tr class="active">
                    <th>Order Num</th>
                    <th>Order Date</th>
                    <th>Customer Name</th>
                    <th>Customer Address</th>
                    <th>Customer Email</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>View</th>
                </tr>
            </thead>
    <tbody>
    </c:if>
    <c:if test="${value==true}">
        <h3>You haven't made any orders</h3>
    </c:if>
    <c:forEach items="${orders}" var="orderInfo" >
        <tr class="active">
            <td>${orderInfo.orderNum}</td>
            <td>
                <fmt:formatDate value="${orderInfo.orderDate}" pattern="dd-MM-yyyy HH:mm"/>
            </td>
            <td>${orderInfo.customerName}</td>
            <td>${orderInfo.customerAddress}</td>
            <td>${orderInfo.customerEmail}</td>
            <td style="color:red;">
                <fmt:formatNumber value="${orderInfo.amount}" type="currency"/>
            </td>
            <td>${orderInfo.status}</td>
            <td><a href="${pageContext.request.contextPath}/order?orderId=${orderInfo.id}">View</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>