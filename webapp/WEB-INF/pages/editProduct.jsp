<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product</title>

</head>
<body>

<jsp:include page="_header.jsp" />
<security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
    <jsp:include page="_menu.jsp" />
</security:authorize>
<div class="container">
<h1>Edit Product</h1>

<form:form modelAttribute="productForm" method="POST" enctype="multipart/form-data" >

    <div class="form-group">
        <label for="code">Code</label>
        <form:input disabled="true" id="code" path="code" value="${productForm.code}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="code" class="error-message" />
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <form:input id="name" path="name" value="${productForm.name}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
        <form:errors path="name" class="error-message" />
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <form:input id="price" path="price" value="${productForm.price}" type="currency" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
        <form:errors path="price" class="error-message" />
    </div>
    <div class="form-group">
        <label for="storage">Storage</label>
        <form:input  id="storage" path="storage" value="${productForm.storage}"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"  min="0" required="true"/>
        <form:errors path="storage" class="error-message" />
    </div>
    <div class="form-group">
        <label for="productType">Category</label>
        <form:input disabled="true" id="productType" path="productType" value="${productForm.productType}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
        <form:errors path="productType" class="error-message" />
    </div>
    <form:input hidden="true" path="productType" value="${productForm.productType}" />

        <c:forEach var="element" items="${attributeList}" varStatus="movieLoopCount2" >
        <c:forEach var="val" items="${arrayList}" varStatus="movieLoopCount"  >
            <c:if  test = "${movieLoopCount.count==movieLoopCount2.count}">
                <div class="form-group">
                    <label for="atrValue">${element}</label>
                    <form:input id="atrValue" path="atrValue" value="${val}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required="true"/>
                </div>
            </c:if>
        </c:forEach>
        </c:forEach>

        Upload new image
        <form:input id="fileData" path="fileData"  type="file" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
    <br>
    <table >
        <td><input type="submit" value="Submit" class="btn btn-primary"/> </td>
        </tr>
    </table>

</form:form>

</div>

</body>
</html>