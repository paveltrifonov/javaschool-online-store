<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap-3.3.7-dist/css/bootstrap.css">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/index">Online Shop</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/index">Home</a></li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Shoes<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <c:forEach var="subcategoties" items="${Shoes}">
                            <li><a href="${pageContext.request.contextPath}/productListCategory?category=${subcategoties.typeName}">${subcategoties.typeName}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Clothing<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <c:forEach var="subcategoties" items="${Clothing}">
                            <li><a href="${pageContext.request.contextPath}/productListCategory?category=${subcategoties.typeName}">${subcategoties.typeName}</a></li>
                        </c:forEach>
                    </ul>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Accessories<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <c:forEach var="subcategoties" items="${Accessories}">
                            <li><a href="${pageContext.request.contextPath}/productListCategory?category=${subcategoties.typeName}">${subcategoties.typeName}</a></li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <security:authorize  access="hasAnyRole('ROLE_USER')">
                    <li> <a href="${pageContext.request.contextPath}/orderUserList"><span class="glyphicon glyphicon-list-alt"></span> Order List</a></li>
                </security:authorize>

                <security:authorize  access="hasAnyRole('ROLE_USER','ROLE_ANONYMOUS')">
                    <li> <a href="${pageContext.request.contextPath}/shoppingCart"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
                </security:authorize>

                <c:if test="${pageContext.request.userPrincipal.name != null}">
                    <li><a href="${pageContext.request.contextPath}/accountInfo"><span class="glyphicon glyphicon-user"></span> ${pageContext.request.userPrincipal.name}</a></li>
                    <li> <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </c:if>

            <c:if test="${pageContext.request.userPrincipal.name == null}">
                <li><a href="${pageContext.request.contextPath}/registration"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="${pageContext.request.contextPath}/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </c:if>

            </ul>
        </div>
    </nav>
<style>
    body {
        background-image: url(background/589440093.jpg); /* Путь к фоновому изображению */
        background-size: 100%;
    }
</style>
