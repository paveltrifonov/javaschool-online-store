package org.o7planning.springmvcshoppingcart.exceptions;

/**
 * Not valid customer information
 */
public class NotValidCustomerException extends Exception {

    public NotValidCustomerException(){super();}

    public NotValidCustomerException(String message){super(message);}
}
