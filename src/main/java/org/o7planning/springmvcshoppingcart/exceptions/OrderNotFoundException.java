package org.o7planning.springmvcshoppingcart.exceptions;

/**
 * Throws if order not found
 */
public class OrderNotFoundException extends Exception {
    public OrderNotFoundException(){super();}

    public OrderNotFoundException(String message) {
        super(message);
    }
}
