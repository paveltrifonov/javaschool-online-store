package org.o7planning.springmvcshoppingcart.exceptions;

/**
 * Throws if not enough products in the storage
 */
public class NotEnoughProductsException extends Exception  {

    public NotEnoughProductsException() {
        super();
    }

    public NotEnoughProductsException(String message) {
        super(message);
    }
}
