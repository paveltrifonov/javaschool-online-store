package org.o7planning.springmvcshoppingcart.exceptions;

/**
 * Throws if product not found
 */
public class ProductNotFoundException extends Exception {
    public ProductNotFoundException() {
        super();
    }

    public ProductNotFoundException(String message) {
        super(message);
    }
}
