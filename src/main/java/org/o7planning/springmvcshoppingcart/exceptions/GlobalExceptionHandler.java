package org.o7planning.springmvcshoppingcart.exceptions;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler for custom exceptions
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = Logger.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(OrderNotFoundException.class)
    public String handleOrderNotFoundException(OrderNotFoundException ex){
        log.error("Order not found");
        return "redirect:/orderList";
    }

    @ExceptionHandler(ProductNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleProductNotFoundException(ProductNotFoundException ex){
        log.error("Product nor found");
    }

    @ExceptionHandler(EmptyCartException.class)
    public String handleEmptyCartException(){
        log.error("Empty cart");
        return "redirect:/shoppingCart";
    }

    @ExceptionHandler(NotValidCustomerException.class)
    public String handleNotValidCustomerException(){
        log.error("Customer information is not valid");
        return "redirect:/shoppingCartCustomer";
    }

}
