package org.o7planning.springmvcshoppingcart.exceptions;

/**
 * Throws if cart is empty
 */
public class EmptyCartException extends Exception {

    public EmptyCartException(){super();}

    public EmptyCartException(String message){super(message);}
}
