package org.o7planning.springmvcshoppingcart.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Fot banner administration
 */
public class BannerListInfo {

    final List<BannerInfo> bannerInfos = new ArrayList<>();

    public List<BannerInfo> getBannerInfos() {
        return bannerInfos;
    }
}
