package org.o7planning.springmvcshoppingcart.model;

import org.o7planning.springmvcshoppingcart.entity.Attribute;

import java.util.LinkedHashSet;

/**
 * Subcategory info
 */

public class ProductTypeInfo {

    private String typeName;
    private String category;
    private LinkedHashSet<Attribute> attributes = new LinkedHashSet<Attribute>();
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public LinkedHashSet<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(LinkedHashSet<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ProductTypeInfo{" +
                "typeName='" + typeName + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
