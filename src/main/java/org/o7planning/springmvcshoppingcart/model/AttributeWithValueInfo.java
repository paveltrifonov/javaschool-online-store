package org.o7planning.springmvcshoppingcart.model;

/**
 * Attribute with value
 */

public class AttributeWithValueInfo {

    private String code;
    private String category;
    private String parameter;

    public AttributeWithValueInfo(){};

    public AttributeWithValueInfo(String code, String category, String parameter) {
        this.code = code;
        this.category = category;
        this.parameter = parameter;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AttributeWithValueInfo{" +
                "code='" + code + '\'' +
                ", category='" + category + '\'' +
                ", parameter='" + parameter + '\'' +
                '}';
    }
}
