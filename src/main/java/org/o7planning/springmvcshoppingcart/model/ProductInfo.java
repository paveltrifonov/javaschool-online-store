package org.o7planning.springmvcshoppingcart.model;

import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Set;

/**
 * Product dto
 */

public class ProductInfo {
    private String code;
    private String name;
    private BigDecimal price;
    private String productType;
    private String category;
    private Integer storage;
    private boolean banner;
    private ArrayList<String> atrValue;

    private boolean newProduct=false;

    // Upload file.
    private CommonsMultipartFile fileData;

    public ProductInfo() {
    }

    public ProductInfo(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.productType = product.getProductType().getTypeName();
        this.storage = product.getStorage();
        this.banner = product.isBanner();
    }


    public ProductInfo(String code, String name, BigDecimal price, ProductType productType, Integer storage, boolean banner) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.productType = productType.getTypeName();
        this.storage = storage;
        this.banner = banner;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public CommonsMultipartFile getFileData() {
        return fileData;
    }

    public void setFileData(CommonsMultipartFile fileData) {
        this.fileData = fileData;
    }

    public boolean isNewProduct() {
        return newProduct;
    }

    public void setNewProduct(boolean newProduct) {
        this.newProduct = newProduct;
    }

    public String getProductType() {
        return productType;
    }

    public ArrayList<String> getAtrValue() {
        return atrValue;
    }

    public void setAtrValue(ArrayList<String> atrValue) {
        this.atrValue = atrValue;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getStorage() {
        return storage;
    }

    public void setStorage(Integer storage) {
        this.storage = storage;
    }

    public boolean isBanner() {
        return banner;
    }

    public void setBanner(boolean banner) {
        this.banner = banner;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", productType='" + productType + '\'' +
                ", category='" + category + '\'' +
                ", storage=" + storage +
                ", newProduct=" + newProduct +
                '}';
    }
}