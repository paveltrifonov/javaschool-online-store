package org.o7planning.springmvcshoppingcart.model;

/**
 * Category
 */

import java.util.Arrays;

public class CategoryInfo {

    private String category;
    private String typeName;
    private String[] attributes;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String[] getAttributes() {
        return attributes;
    }

    public void setAttributes(String[] attributes) {
        this.attributes = attributes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "CategoryInfo{" +
                "category='" + category + '\'' +
                ", typeName='" + typeName + '\'' +
                ", attributes=" + Arrays.toString(attributes) +
                '}';
    }
}
