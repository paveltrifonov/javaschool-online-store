package org.o7planning.springmvcshoppingcart.model;

/**
 * For banner administration
 */
public class BannerInfo {
    String code;
    boolean banner;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isBanner() {
        return banner;
    }

    public void setBanner(boolean banner) {
        this.banner = banner;
    }
}
