package org.o7planning.springmvcshoppingcart.model;

import java.math.BigDecimal;

public class BannerProduct {

    private String code;

    private String name;

    private BigDecimal price;

    private byte[] image;

    public BannerProduct(String code, String name, BigDecimal price, byte[] image) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "BannerProduct{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
