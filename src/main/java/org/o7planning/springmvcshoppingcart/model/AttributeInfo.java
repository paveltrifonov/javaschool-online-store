package org.o7planning.springmvcshoppingcart.model;

import java.util.LinkedHashSet;

/**
 * Attribute dto
 */

public class AttributeInfo {

    private String attributeName;

    private LinkedHashSet<FilterAtributeValue> attributeValues;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public LinkedHashSet<FilterAtributeValue> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(LinkedHashSet<FilterAtributeValue> attributeValues) {
        this.attributeValues = attributeValues;
    }

    @Override
    public String toString() {
        return attributeName;
    }
}
