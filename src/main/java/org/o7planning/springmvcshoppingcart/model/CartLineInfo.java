package org.o7planning.springmvcshoppingcart.model;

/**
 * Position in cart
 */

import java.math.BigDecimal;

public class CartLineInfo {

    private ProductInfo productInfo;
    private int quantity;

    public CartLineInfo() {
        this.quantity = 0;
    }

    public ProductInfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return this.productInfo.getPrice().multiply(new BigDecimal(this.quantity));
    }

    @Override
    public String toString() {
        return "CartLineInfo{" +
                "productInfo=" + productInfo +
                ", quantity=" + quantity +
                '}';
    }
}