package org.o7planning.springmvcshoppingcart.model;

/**
 * Dto for filter products
 */

import org.o7planning.springmvcshoppingcart.entity.Product;

import java.util.Objects;

public class FilterAtributeValue {

    private String attributeValueName;

    private Product product;

    public FilterAtributeValue(String attributeValueName, Product product) {
        this.attributeValueName = attributeValueName;
        this.product = product;
    }

    public String getAttributeValueName() {
        return attributeValueName;
    }

    public void setAttributeValueName(String attributeValueName) {
        this.attributeValueName = attributeValueName;
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterAtributeValue that = (FilterAtributeValue) o;
        return Objects.equals(attributeValueName, that.attributeValueName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(attributeValueName);
    }

    @Override
    public String toString() {
        return "FilterAtributeValue{" +
                "attributeValueName='" + attributeValueName + '\'' +
                ", product=" + product +
                '}';
    }
}
