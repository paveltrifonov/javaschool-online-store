package org.o7planning.springmvcshoppingcart.model;

import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.Product;

/**
 * AttributeValue dto
 */

public class AttributeValueInfo {

    private Integer id;
    private String attributeValueName;
    private Product product;
    private Attribute attribute;

    public String getAttributeValueName() {
        return attributeValueName;
    }

    public void setAttributeValueName(String attributeValueName) {
        this.attributeValueName = attributeValueName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AttributeValueInfo{" +
                "attributeValueName='" + attributeValueName + '\'' +
                ", product=" + product +
                ", attribute=" + attribute +
                '}';
    }
}
