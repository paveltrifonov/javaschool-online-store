package org.o7planning.springmvcshoppingcart.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import javax.persistence.*;

/**
 * Product
 */

@NamedQueries({
        @NamedQuery(
                name = "bannerProducts",
                query = "select new org.o7planning.springmvcshoppingcart.model.BannerProduct(p.code, p.name, p.price, p.image) from Product p where p.banner = true"),
        @NamedQuery(
                name = "productBySubcategory",
                query = "select new org.o7planning.springmvcshoppingcart.model.ProductInfo(p.code, p.name, p.price, p.productType, p.storage, p.banner) from Product p where p.productType = :productType"),
        @NamedQuery(
                name = "findProduct",
                query = "select p from Product p where p.code = :code"),
        @NamedQuery(
                name = "findProducts",
                query = "select new org.o7planning.springmvcshoppingcart.model.ProductInfo(p.code, p.name, p.price, p.productType, p.storage, p.banner) from Product p order by p.createDate desc "),
        @NamedQuery(
                name = "findProductsWithName",
                query = "select new org.o7planning.springmvcshoppingcart.model.ProductInfo(p.code, p.name, p.price, p.productType, p.storage, p.banner)from Product p where lower(p.name) like :likeName order by p.createDate desc ")
})

@Entity
@Table(name = "Products")
public class Product implements Serializable {

    private static final long serialVersionUID = -1000119078147252957L;

    @Id
    @Column(name = "Code", length = 20, nullable = false)
    private String code;

    @Column(name = "Name", length = 255, nullable = false)
    private String name;

    @Column(name = "Price", nullable = false)
    private BigDecimal price;

    @Column(name = "Storage", nullable = false)
    private Integer storage;

    @Column(name = "Banner", nullable = false)
    private boolean banner;

    @Lob
    @Column(name = "Image", length = Integer.MAX_VALUE, nullable = true)
    private byte[] image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_type_id", nullable = false)
    private ProductType productType;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Create_Date", nullable = false)
    private Date createDate;

    public Product() {
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the date to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the image
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * @return the subcategory of product
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * @param productType the subcategory of product
     */
    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    /**
     * @return the storage
     */
    public Integer getStorage() {
        return storage;
    }

    /**
     * @param storage the storage to set
     */
    public void setStorage(Integer storage) {
        this.storage = storage;
    }

    /**
     * @return true if product must should be shown in the second project
     */
    public boolean isBanner() {
        return banner;
    }

    /**
     * @param banner the banner to set
     */
    public void setBanner(boolean banner) {
        this.banner = banner;
    }

    @Override
    public String toString() {
        return "Product{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", storage=" + storage +
                ", productType=" + productType +
                ", createDate=" + createDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(getCode(), product.getCode()) &&
                Objects.equals(getName(), product.getName()) &&
                Objects.equals(getCreateDate(), product.getCreateDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCode(), getName(), getCreateDate());
    }
}
