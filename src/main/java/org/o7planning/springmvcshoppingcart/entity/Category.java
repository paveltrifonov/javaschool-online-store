package org.o7planning.springmvcshoppingcart.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Category
 */
@NamedQueries({
        @NamedQuery(
                name = "findCategory",
                query = "Select c from Category c where c.categoryName = :categoryName"),
        @NamedQuery(
                name = "findAllCategory",
                query = "Select c from Category c order by c.categoryName")
})

@Entity
@Table(name = "category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "category_name")
    private String categoryName;

    @OneToMany(mappedBy="category", fetch = FetchType.LAZY)
    private List<ProductType> productTypes;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name of category
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName the name of category to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the list of products in this category
     */
    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    /**
     * @param productTypes the list of products to set
     */
    public void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    @Override
    public String toString() {
        return categoryName;
    }
}
