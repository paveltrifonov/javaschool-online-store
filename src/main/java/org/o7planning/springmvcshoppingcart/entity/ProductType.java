package org.o7planning.springmvcshoppingcart.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Subcategory
 */

@NamedQueries({
        @NamedQuery(
                name = "findProductType",
                query = "select p from ProductType p where p.typeName = :typeName"),
        @NamedQuery(
                name = "findAllProductType",
                query = "select p from ProductType p order by p.typeName")
})

@Entity
@Table(name = "product_type")
public class ProductType implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Typet_Name", nullable = false)
    private String typeName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @ManyToMany(fetch = FetchType.EAGER,cascade = { CascadeType.ALL })
    @JoinTable(
            name = "product_type_has_attribute",
            joinColumns = { @JoinColumn(name = "product_type_id") },
            inverseJoinColumns = { @JoinColumn(name = "attribute_id") }
    )
    @OrderBy(value = "attribute_name")
    Set<Attribute> attributes= new LinkedHashSet<Attribute>();

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id th id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name of subcategory
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the name of subcategory to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the set of attributes
     */
    public Set<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the set of attributes
     */
    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return typeName;
    }
}
