package org.o7planning.springmvcshoppingcart.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * Order
 */
@NamedQueries({
        @NamedQuery(
                name = "maxOrderNum",
                query = "Select max(o.orderNum) from Order o "),
        @NamedQuery(
                name = "orders",
                query = "Select new org.o7planning.springmvcshoppingcart.model.OrderInfo(ord.id, ord.orderDate, ord.orderNum, " +
                        "ord.amount, ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone, ord.status)" +
                        " from Order ord order by ord.orderNum desc"),
        @NamedQuery(
                name = "orderDetails",
                query = "Select new org.o7planning.springmvcshoppingcart.model.OrderDetailInfo(d.id, d.product.code, d.product.name , d.quanity,d.price,d.amount) " +
                        "from OrderDetail d where d.order.id = :orderId "),
        @NamedQuery(
                name = "findOrder",
                query = "Select ord from Order ord where ord.id = :orderId")
})

@Entity
@Table(name = "Orders", //
        uniqueConstraints = { @UniqueConstraint(columnNames = "Order_Num") })
public class Order implements Serializable {

    private static final long serialVersionUID = -2576670215015463100L;

    @Id
    @Column(name = "ID", length = 50)
    private String id;

    @Column(name = "Order_Date", nullable = false)
    private Date orderDate;

    @Column(name = "Order_Num", nullable = false)
    private int orderNum;

    @Column(name = "Amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "Customer_Name", length = 255, nullable = false)
    private String customerName;

    @Column(name = "Customer_Address", length = 255, nullable = false)
    private String customerAddress;

    @Column(name = "Customer_Email", length = 128, nullable = false)
    private String customerEmail;

    @Column(name = "Customer_Phone", length = 128, nullable = false)
    private String customerPhone;

    @Column(name = "Status", length = 128, nullable = false)
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "accounts_User_Name", nullable = false)
    private Account account;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the date of order
     */
    public Date getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the date of order to set
     */
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the number of order
     */
    public int getOrderNum() {
        return orderNum;
    }

    /**
     * @param orderNum th number of order to set
     */
    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the customer name
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customer name to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customer address
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * @param customerAddress the customer address to set
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * @return the customer email
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * @param customerEmail the customer email to set
     */
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    /**
     * @return the customer phone
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * @param customerPhone the customer phone to set
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * @return the status of order
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status of order to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the account of customer
     */
    public Account getAccount() {
        return account;
    }

    /**
     * @param account the account of customer to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", orderDate=" + orderDate +
                ", orderNum=" + orderNum +
                ", amount=" + amount +
                ", customerName='" + customerName + '\'' +
                ", customerAddress='" + customerAddress + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", customerPhone='" + customerPhone + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
