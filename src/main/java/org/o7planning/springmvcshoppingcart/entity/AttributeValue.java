package org.o7planning.springmvcshoppingcart.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Parameter value
 */

@NamedQueries({
        @NamedQuery(
                name = "findAttributeValue",
                query = "Select val from AttributeValue val where val.id = :id"),

        @NamedQuery(
                name = "findAllValues",
                query = "Select val from AttributeValue val order by val.id")
})

@Entity
@Table(name = "attribute_value")
public class AttributeValue implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "attribute_value_name", nullable = false)
    private String attributeValueName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "products_Code", nullable = false)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attribute_id", nullable = false)
    private Attribute attribute;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the parameter value
     */
    public String getAttributeValueName() {
        return attributeValueName;
    }

    /**
     * @param attributeValueName the parameter value to set
     */
    public void setAttributeValueName(String attributeValueName) {
        this.attributeValueName = attributeValueName;
    }

    /**
     * @return the product with this parameter value
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the parameter
     */
    public Attribute getAttribute() {
        return attribute;
    }

    /**
     * @param attribute the parameter to set
     */
    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    @Override
    public String toString() {
        return "AttributeValue{" +
                "id=" + id +
                ", attributeValueName='" + attributeValueName + '\'' +
                ", product=" + product +
                ", attribute=" + attribute +
                '}';
    }
}
