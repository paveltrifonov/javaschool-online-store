package org.o7planning.springmvcshoppingcart.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Product parameter
 */

@NamedQueries({
        @NamedQuery(
                name = "findAttribute",
                query = "Select atr from Attribute atr where atr.attributeName = :attributeName"),
        @NamedQuery(
                name = "findAllAttribute",
                query = "Select atr from Attribute atr order by atr.attributeName")
})

@Entity
@Table(name = "attribute")
public class Attribute implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "attribute_name", nullable = false)
    private String attributeName;

    @OneToMany(mappedBy="attribute", fetch = FetchType.LAZY)
    private List<AttributeValue> attributeValues;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * @return the list of attribute values
     */
    public List<AttributeValue> getAttributeValues() {
        return attributeValues;
    }

    /**
     * @param attributeValues the list of attribute values to set
     */
    public void setAttributeValues(List<AttributeValue> attributeValues) {
        this.attributeValues = attributeValues;
    }

    @Override
    public String toString() {
        return attributeName;
    }
}
