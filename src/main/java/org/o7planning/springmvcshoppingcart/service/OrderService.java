package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.Order;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.CartInfo;
import org.o7planning.springmvcshoppingcart.model.OrderDetailInfo;
import org.o7planning.springmvcshoppingcart.model.OrderInfo;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Order service
 */
@Service
public interface OrderService {

    /**
     * Save order
     * @param cartInfo the cart
     */
    void saveOrder(CartInfo cartInfo);

    /**
     * Get order by id
     * @param orderId the id of order
     * @return Order object
     */
    Order findOrder(String orderId);

    /**
     * Update status of order
     * @param id the id of order
     * @param status new status of order
     */
    void updateStatus(String id, String status);

    /**
     * Page list of orders
     */
    PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage);

    /**
     * Get order information
     * @param orderId order id
     * @return OrderInfo object
     * @throws OrderNotFoundException if order not found
     */
    OrderInfo getOrderInfo(String orderId) throws OrderNotFoundException;

    /**
     * Get list details of order
     * @param orderId order id
     */
    List<OrderDetailInfo> listOrderDetailInfos(String orderId);
}
