package org.o7planning.springmvcshoppingcart.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Email Service
 */

@Service("emailService")
public class EmailService {

    private static final Logger log = Logger.getLogger(EmailService.class);

    @Autowired
    private MailSender mailSender;
    @Autowired
    private SimpleMailMessage mailMessage;

    /**
     * Sending email
     * @param email the email
     * @param orderNum num of order
     */
    public void sendMail(String email, Integer orderNum){
        log.info("Send email to: " + email);
        SimpleMailMessage msg = new SimpleMailMessage(mailMessage);
        msg.setTo(email);
        msg.setSubject("Online Shop");
        msg.setText("Your order has been successfully paid. Order number is "+orderNum);
        mailSender.send(msg);
    }
}
