package org.o7planning.springmvcshoppingcart.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

@Component
public class MessageSender {

    private static final Logger log = Logger.getLogger(MessageSender.class);

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMessage() {
        log.info("Sending message");
        jmsTemplate.send(new MessageCreator(){
            @Override
            public TextMessage createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage("Hello");
                return textMessage;
            }
        });
    }

}
