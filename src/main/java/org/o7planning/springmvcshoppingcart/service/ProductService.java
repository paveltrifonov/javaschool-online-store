package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.exceptions.NotEnoughProductsException;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.*;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Product service
 */
@Service
public interface ProductService {

    /**
     * Get product by code
     * @param code code of product
     * @return product
     */
    Product findProduct(String code) throws ProductNotFoundException;

    /**
     * Get product in ProductInfo
     * @param code product code
     */
    ProductInfo findProductInfo(String code);

    /**
     * Get list of products by category
     * @param productType subcategory
     * @return list of products
     */
    List<ProductInfo> getProductsBySubcategory(String productType);

    /**
     * Get list of products for banner
     */
    List<BannerProduct> getBannerProducts();

    /**
     * Page list of products
     */
    PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage  );

    /**
     * Page list of products
     * @param likeName name of product
     * @return
     */
    PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName);

    /**
     * Get necessary products
     * @param filteredProducts filterable products
     * @param attributes filtering parameters
     * @param filterProduct filtered products
     * @return list of filtered products
     */
    List<ProductInfo> filterProducts(List<ProductInfo> filteredProducts, List<AttributeWithValueInfo> attributes, ProductInfo filterProduct);

    /**
     * Check number of products of cart in the storage
     * @param cartInfo the cart
     * @throws NotEnoughProductsException if not enough number of products in storage
     * @throws ProductNotFoundException if products not found
     */
    void checkCart(List<CartLineInfo> cartInfo)  throws NotEnoughProductsException, ProductNotFoundException;

    /**
     * Save or update product
     */
    void save(ProductInfo productInfo);
}
