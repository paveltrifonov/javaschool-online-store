package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.model.AccountInfo;
import org.o7planning.springmvcshoppingcart.model.ChangePasswordInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;
import org.springframework.stereotype.Service;

/**
 * Account service
 */

@Service
public interface AccountService {
    /**
     * Find account by user name( used to login)
     * @param userName user name for find account
     * @return account
     */
    Account findAccount(String userName );
    /**
     * Update old or save new account
     * @param accountInfo information to save or update account
     */
    void saveAccount(AccountInfo accountInfo);
    /**
     * Change password
     * @param changePasswordInfo information to change password
     */
    void saveAccount(ChangePasswordInfo changePasswordInfo);

    /**
     * Check userName for uniqueness
     * @param userName user name
     */
    boolean isUserNameUnique(String userName);
    /**
     * Change customer info
     * @param customerInfo information to change customer info
     */
    void changeAccountInfo(CustomerInfo customerInfo);
}
