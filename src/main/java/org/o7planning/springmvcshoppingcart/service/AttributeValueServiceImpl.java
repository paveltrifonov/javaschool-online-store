package org.o7planning.springmvcshoppingcart.service;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.dao.AttributeValueDAO;
import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.AttributeValueInfo;
import org.o7planning.springmvcshoppingcart.model.AttributeWithValueInfo;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Implementation of attribute value service
 */
@Transactional
@Service("attributeValueService")
public class AttributeValueServiceImpl implements AttributeValueService {

    private static final Logger log = Logger.getLogger(AttributeValueServiceImpl.class);

    @Autowired
    AttributeValueDAO attributeValueDAO;

    @Autowired
    ProductTypeService productTypeService;

    @Autowired
    ProductService productService;

    @Override
    public AttributeValue findAttributeValue(Integer id) {
        return attributeValueDAO.findAttributeValue(id);
    }

    @Override
    public LinkedHashSet<AttributeValue> findAll() {
        return attributeValueDAO.findAll();
    }

    @Override
    public List<AttributeWithValueInfo> getAttributes(List<ProductInfo> productList) {
        log.debug("Get products attribute values");
        List<AttributeWithValueInfo> attributes = new ArrayList<AttributeWithValueInfo>();
        LinkedHashSet<AttributeValue> attributeValues = findAll();
        List<ProductInfo> productInfoList = productList;
        for (ProductInfo productInfo : productInfoList) {
            for (AttributeValue attributeValue : attributeValues) {
                if (attributeValue.getProduct().getCode().equals(productInfo.getCode())) {
                    attributes.add(new AttributeWithValueInfo(productInfo.getCode(), attributeValue.getAttribute().getAttributeName(), attributeValue.getAttributeValueName()));
                }
            }
        }
        return attributes;
    }

    @Override
    public void saveAttributeValue(AttributeValueInfo attributeValueInfo) {
        attributeValueDAO.saveAttributeValue(attributeValueInfo);
    }

    @Override
    public void saveNewAttributeValue(ProductInfo productInfo) throws ProductNotFoundException {
        log.debug("Update product attribute values");
        ArrayList<Attribute> attributes = new ArrayList<Attribute>(productTypeService.getCategoryAttributes(productInfo.getProductType()));
        AttributeValueInfo attributeValueInfo = new AttributeValueInfo();
        ArrayList<String> arrayList = productInfo.getAtrValue();
        LinkedHashSet<AttributeValue> attributeValues = findAll();
        int i = 0;
        for(AttributeValue attributeValue: attributeValues) {
            if (attributeValue.getProduct().getCode().equals(productInfo.getCode())) {
                attributeValueInfo.setId(attributeValue.getId());
                attributeValueInfo.setAttributeValueName(arrayList.get(i));
                attributeValueInfo.setAttribute(attributes.get(i));
                attributeValueInfo.setProduct(productService.findProduct(productInfo.getCode()));
                saveAttributeValue(attributeValueInfo);
                i++;
            }
        }
    }
}
