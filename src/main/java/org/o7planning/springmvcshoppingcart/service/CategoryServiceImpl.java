package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.dao.CategoryDAO;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;

/**
 * Implementation of category service
 */
@Transactional
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public Category findCategory(String categoryName) {
        return categoryDAO.findCategory(categoryName);
    }

    @Override
    public LinkedHashSet<Category> findAll() {
        return categoryDAO.findAll();
    }

    @Override
    public LinkedHashSet<ProductType> getProductTypes(String categoryName) {
        return categoryDAO.getProductTypes(categoryName);
    }

    @Override
    public void saveCategory(Category category) {
        categoryDAO.saveCategory(category);
    }
}
