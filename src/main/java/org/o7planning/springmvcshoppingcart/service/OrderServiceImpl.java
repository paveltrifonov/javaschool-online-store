package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.dao.OrderDAO;
import org.o7planning.springmvcshoppingcart.entity.Order;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.CartInfo;
import org.o7planning.springmvcshoppingcart.model.OrderDetailInfo;
import org.o7planning.springmvcshoppingcart.model.OrderInfo;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

 /**
  *  Order service implementation
  */
@Transactional
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDAO orderDAO;

    public void saveOrder(CartInfo cartInfo){
        orderDAO.saveOrder(cartInfo);
    }

     @Override
     public Order findOrder(String orderId) {
         return orderDAO.findOrder(orderId);
     }

     public void updateStatus(String id, String status){
        orderDAO.updateStatus(id, status);
    }

    public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage){
       return orderDAO.listOrderInfo(page, maxResult,maxNavigationPage);
    }

    public OrderInfo getOrderInfo(String orderId) throws OrderNotFoundException {
        return orderDAO.getOrderInfo(orderId);
    }

    public List<OrderDetailInfo> listOrderDetailInfos(String orderId){
        return orderDAO.listOrderDetailInfos(orderId);
    }

}
