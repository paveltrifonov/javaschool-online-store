package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;

/**
 * Category service
 */
@Service
public interface CategoryService {
    /**
     * Get category by name
     * @param categoryName category name
     * @return category
     */
    Category findCategory(String categoryName);
    /**
     * Get all categories
     */
    LinkedHashSet<Category> findAll();
    /**
     * Get subcategories of category
     * @param categoryName category name
     * @return set of subcategories
     */
    LinkedHashSet<ProductType> getProductTypes(String categoryName);
    /**
     * Save new category
     */
    void saveCategory(Category category);

}
