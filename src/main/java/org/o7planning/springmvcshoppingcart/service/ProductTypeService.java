package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.model.AttributeInfo;
import org.o7planning.springmvcshoppingcart.model.ProductTypeInfo;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Subcategory service
 */
@Service
public interface ProductTypeService {

    /**
     * Get product subcategory
     * @param typeName name of subcategory
     */
    ProductType findProductType(String typeName);

    /**
     * Get all subcategories
     * @return set of subcategories
     */
    LinkedHashSet<ProductType> findAll();

    /**
     * Get category of subcategory
     * @param typeName subcategory name
     * @return category
     */
    Category getCategory(String typeName);

    /**
     * Get subcategory parameters
     * @param typeName subcategory name
     * @return set of parameters
     */
    LinkedHashSet<Attribute> getCategoryAttributes(String typeName);

    /**
     * Get filtered values by subcategory name
     * @param typename name of subcategory
     */
    List<AttributeInfo> getFilteredValues(String typename);
    /**
     * Save subcategory
     * @param productTypeInfo subcategory information
     */
    void saveProductType(ProductTypeInfo productTypeInfo);
}
