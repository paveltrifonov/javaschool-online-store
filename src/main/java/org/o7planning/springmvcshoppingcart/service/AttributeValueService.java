package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.AttributeValueInfo;
import org.o7planning.springmvcshoppingcart.model.AttributeWithValueInfo;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;
/**
 * Parameters value service
 */
@Service
public interface AttributeValueService {

    /**
     * Get parameter value by id
     * @param id the id of parameter value
     * @return
     */
    AttributeValue findAttributeValue(Integer id);

    /**
     * Get all values
     * @return set of values
     */
    LinkedHashSet<AttributeValue> findAll();

    /**
     * Get products parameters with values
     * @param productList list of products
     * @return list of parameters with values
     */
    List<AttributeWithValueInfo> getAttributes(List<ProductInfo> productList );

    /**
     * Save parameter value
     */
    void saveAttributeValue(AttributeValueInfo attributeValueInfo);

    /**
     * Save new parameter value of product
     * @param productInfo
     * @throws ProductNotFoundException
     */
    void saveNewAttributeValue(ProductInfo productInfo) throws ProductNotFoundException;
}
