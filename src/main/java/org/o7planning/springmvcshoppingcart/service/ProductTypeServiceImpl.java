package org.o7planning.springmvcshoppingcart.service;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.dao.ProductTypeDAO;
import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.model.AttributeInfo;
import org.o7planning.springmvcshoppingcart.model.FilterAtributeValue;
import org.o7planning.springmvcshoppingcart.model.ProductTypeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * ProductTypeService implementation
 */
@Transactional
@Service("productTypeService")
public class ProductTypeServiceImpl implements ProductTypeService {

    private static final Logger log = Logger.getLogger(ProductTypeServiceImpl.class);

    @Autowired
    private ProductTypeDAO productTypeDAO;

    @Override
    public ProductType findProductType(String typeName) {
        return productTypeDAO.findProductType(typeName);
    }

    @Override
    public LinkedHashSet<ProductType> findAll() {
        return productTypeDAO.findAll();
    }

    @Override
    public Category getCategory(String typeName) {
        return productTypeDAO.getCategory(typeName);
    }

    @Override
    public LinkedHashSet<Attribute> getCategoryAttributes(String typeName) {
        return productTypeDAO.getCategoryAttributes(typeName);
    }

    @Override
    public List<AttributeInfo> getFilteredValues(String typename) {
        log.debug("Get filtered attribute values");
        LinkedHashSet<Attribute> categoryAttribute = getCategoryAttributes(typename);
        List<AttributeInfo> attributeInfos = new ArrayList<>();
        for(Attribute attribute: categoryAttribute){
            AttributeInfo attributeInfo = new AttributeInfo();
            attributeInfo.setAttributeName(attribute.getAttributeName());
            LinkedHashSet<FilterAtributeValue> linkedHashSet = new LinkedHashSet<>();
            for(AttributeValue attributeValue: attribute.getAttributeValues()){
                if(attributeValue.getProduct().getStorage()!=0 && attributeValue.getProduct().getProductType().getTypeName().equals(typename)) {
                    linkedHashSet.add(new FilterAtributeValue(attributeValue.getAttributeValueName(), attributeValue.getProduct()));
                }
            }
            attributeInfo.setAttributeValues(linkedHashSet);
            attributeInfos.add(attributeInfo);
        }
        return attributeInfos;
    }

    @Override
    public void saveProductType(ProductTypeInfo productTypeInfo) {
        productTypeDAO.saveProductType(productTypeInfo);

    }
}
