package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.dao.AttributeDAO;
import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.model.AttributeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of attribute service
 */

@Transactional
@Service("attributeService")
public class AttributeServiceImpl implements AttributeService  {

    @Autowired
    private AttributeDAO attributeDAO;

    @Override
    public Attribute findAttribute(String attributeName) {
        return attributeDAO.findAttribute(attributeName);
    }

    @Override
    public LinkedHashSet<Attribute> findAll() {
        return attributeDAO.findAll();
    }

    @Override
    public List<AttributeValue> findValues(String attributeName) {
        return attributeDAO.findValues(attributeName);
    }

    @Override
    public void saveAttribute(AttributeInfo attributeInfo) {
        attributeDAO.saveAttribute(attributeInfo);
    }


}
