package org.o7planning.springmvcshoppingcart.service;

import org.o7planning.springmvcshoppingcart.dao.AccountDAO;
import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.model.AccountInfo;
import org.o7planning.springmvcshoppingcart.model.ChangePasswordInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of account service
 */

@Transactional
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Account findAccount(String userName ) {
        return accountDAO.findAccount(userName);
    }


    public void saveAccount(AccountInfo accountInfo){
        accountInfo.setPassword(passwordEncoder.encode(accountInfo.getPassword()));
        accountDAO.saveAccount(accountInfo);
    }

    public void saveAccount(ChangePasswordInfo changePasswordInfo){
        changePasswordInfo.setNewPassword(passwordEncoder.encode(changePasswordInfo.getNewPassword()));
        accountDAO.saveAccount(changePasswordInfo);
    }

    public boolean isUserNameUnique(String userName){
        Account account = findAccount(userName);
        return account!= null;
    }

    @Override
    public void changeAccountInfo(CustomerInfo customerInfo) {
        accountDAO.changeAccountInfo(customerInfo);
    }

    public void setAccountDAO(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
