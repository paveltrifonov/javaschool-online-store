package org.o7planning.springmvcshoppingcart.service;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.dao.ProductDAO;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.exceptions.NotEnoughProductsException;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Product service implementation
 */
@Transactional
@Service("productService")
public class ProductServiceImpl implements ProductService{

    private static final Logger log = Logger.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductDAO productDAO;

    public Product findProduct(String code) throws ProductNotFoundException{
        Product product = productDAO.findProduct(code);
        if (product == null) {
            log.error("Product with code " + code + " not found");
            throw new ProductNotFoundException("Product with code " + code + " not found");
        }
        return product;
    }

    public ProductInfo findProductInfo(String code){
        return productDAO.findProductInfo(code);
    }

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage  ){
        return productDAO.queryProducts(page,maxResult,maxNavigationPage);
    }

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName){
        return productDAO.queryProducts(page, maxResult, maxNavigationPage, likeName);
    }

    @Override
    public List<ProductInfo> getProductsBySubcategory(String productType) {
        return productDAO.getProductsBySubcategory(productType);
    }

    @Override
    public List<BannerProduct> getBannerProducts() {
        return productDAO.getBannerParoducts();
    }

    @Override
    public List<ProductInfo> filterProducts(List<ProductInfo> products, List<AttributeWithValueInfo> attributes, ProductInfo filterProduct) {
        log.debug("Product filtration");
        List<ProductInfo> filteredProducts = products;
        List<ProductInfo> trueProducts = new ArrayList<>();
        for (ProductInfo productInfo: filteredProducts){
            ArrayList<String> str = new ArrayList<>();
            for (AttributeWithValueInfo attributeWithValueInfo: attributes){
                if(attributeWithValueInfo.getCode().equals(productInfo.getCode())){
                    str.add(attributeWithValueInfo.getParameter());
                }
            }
            productInfo.setAtrValue(str);
        }

        for (ProductInfo productInfo: filteredProducts){
            Boolean flag=true;
            if(productInfo.getProductType().equals(filterProduct.getCategory())){
                List<String> values = productInfo.getAtrValue();
                List<String> filterValues = filterProduct.getAtrValue();
                for (int i = 0; i < values.size(); i++) {
                    if (!(values.get(i).equals(filterValues.get(i)) || filterValues.get(i).equals(""))) {
                        flag = false;
                    }
                }
            }
            if(flag){ trueProducts.add(productInfo);
            }
        }
        return trueProducts;
    }

    @Override
    public void checkCart(List<CartLineInfo> cartInfo) throws NotEnoughProductsException, ProductNotFoundException {
        log.debug("Check products storage in cart");

        for(CartLineInfo cartLineInfo: cartInfo){
            if((findProduct(cartLineInfo.getProductInfo().getCode()).getStorage() - cartLineInfo.getQuantity())<0){
                log.error("Not enough quantity of product in storage");
                throw new NotEnoughProductsException("Not enough quantity of " + cartLineInfo.getProductInfo().getName() + " in storage");
            }
        }
            log.debug("Number of products in the storage is enough to buy");
            for(CartLineInfo cartLineInfo: cartInfo){
                cartLineInfo.getProductInfo().setStorage(cartLineInfo.getProductInfo().getStorage() - cartLineInfo.getQuantity());
                save(cartLineInfo.getProductInfo());
            }
    }

    public void save(ProductInfo productInfo){
        productDAO.save(productInfo);
    }
}
