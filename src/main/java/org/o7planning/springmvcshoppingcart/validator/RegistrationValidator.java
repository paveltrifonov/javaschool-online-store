package org.o7planning.springmvcshoppingcart.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.validator.routines.EmailValidator;
import org.o7planning.springmvcshoppingcart.model.AccountInfo;
import org.o7planning.springmvcshoppingcart.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for registration page
 */
@Component
public class RegistrationValidator implements Validator {

    @Autowired
    private AccountService accountService;

    private Pattern pattern;
    private Matcher matcher;
    private EmailValidator emailValidator = EmailValidator.getInstance();

    @Override
    public boolean supports(Class<?> aClass) {
        return AccountInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        AccountInfo accountInfo = (AccountInfo) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty.registrationForm.userName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.registrationForm.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.customerForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty.customerForm.address");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.customerForm.phone");

        if ((accountInfo.getUserName().length() < 6 || accountInfo.getUserName().length() > 15) & accountInfo.getUserName().length() != 0) {
            errors.rejectValue("userName", "Wrong.length.userName");
        }


        if (!emailValidator.isValid(accountInfo.getEmail())) {
            errors.rejectValue("email", "Pattern.customerForm.email");
        }

        if (accountInfo.getPassword().length() < 8 & !accountInfo.getPassword().isEmpty()) {
            errors.rejectValue("password", "Wrong.length.password");
        }

        if (!accountInfo.getPassword().equals(accountInfo.getMatchingPassword())) {
            errors.rejectValue("matchingPassword", "Password.not.matches");
        }

        if (accountService.isUserNameUnique(accountInfo.getUserName())) {
            errors.rejectValue("userName", "Username.is.not.unique");
        }

        pattern = Pattern.compile("[0-9+]{11}");
        matcher = pattern.matcher(accountInfo.getPhone());
        if (!matcher.matches() & !accountInfo.getPhone().isEmpty()) {
            errors.rejectValue("phone", "Phone.incorrect");

        }
    }
}