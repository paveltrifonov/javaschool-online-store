package org.o7planning.springmvcshoppingcart.validator;

import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.model.ChangePasswordInfo;
import org.o7planning.springmvcshoppingcart.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for passwords
 */
@Component
public class ChangePasswordValidator implements Validator {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AccountService accountService;

    @Override
    public boolean supports(Class<?> aClass) {
        return ChangePasswordInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ChangePasswordInfo changePasswordInfo = (ChangePasswordInfo) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty.changePassword.newPassword");

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Account account = accountService.findAccount(userDetails.getUsername());



        if(! passwordEncoder.matches(changePasswordInfo.getOldPassword(),account.getPassword())){
            errors.rejectValue("oldPassword", "Wrong.old.password");
        }

        if (changePasswordInfo.getNewPassword().length() < 8 & !changePasswordInfo.getNewPassword().isEmpty()) {
            errors.rejectValue("newPassword", "Wrong.length.password");
        }

        if (!changePasswordInfo.getNewPassword().equals(changePasswordInfo.getMatchingPassword())) {
            errors.rejectValue("matchingPassword", "Password.not.matches");
        }

        if (changePasswordInfo.getNewPassword().equals(changePasswordInfo.getOldPassword()) & !changePasswordInfo.getNewPassword().isEmpty()) {
            errors.rejectValue("newPassword", "Password.matches");
        }

    }
}