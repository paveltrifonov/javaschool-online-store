package org.o7planning.springmvcshoppingcart.validator;

import org.o7planning.springmvcshoppingcart.model.CategoryInfo;
import org.o7planning.springmvcshoppingcart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for subcategories
 */
@Component
public class CategoryInfoValidator implements Validator {

    @Autowired
    private ProductTypeService productTypeService;

    @Override
    public boolean supports(Class<?> aClass) {
        return CategoryInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CategoryInfo categoryInfo = (CategoryInfo) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "typeName", "NotEmpty.typeName");

        if(productTypeService.findProductType(categoryInfo.getTypeName())!=null){
            errors.rejectValue("typeName", "TypeName.is.not.unique");
        }
    }
}
