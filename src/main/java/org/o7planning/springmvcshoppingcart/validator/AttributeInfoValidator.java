package org.o7planning.springmvcshoppingcart.validator;

import org.o7planning.springmvcshoppingcart.model.AttributeInfo;
import org.o7planning.springmvcshoppingcart.service.AttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for parameters
 */
@Component
public class AttributeInfoValidator implements Validator {

    @Autowired
    private AttributeService attributeService;

    @Override
    public boolean supports(Class<?> aClass) {
        return AttributeInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        AttributeInfo attributeInfo = (AttributeInfo) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "attributeName", "NotEmpty.attributeName");

        if(attributeService.findAttribute(attributeInfo.getAttributeName())!=null){
            errors.rejectValue("attributeName", "AttributeName.is.not.unique");
        }
    }
}
