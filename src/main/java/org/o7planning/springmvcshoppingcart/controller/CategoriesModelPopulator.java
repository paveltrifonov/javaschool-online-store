package org.o7planning.springmvcshoppingcart.controller;

import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

/**
 * Gives models for product menu
 */
@ControllerAdvice
public class CategoriesModelPopulator {

    @Autowired
    CategoryService categoryService;

    @ModelAttribute
    public void populateCategories(Model model) {
        Category category = categoryService.findCategory("Shoes");
        List<ProductType> shoes = category.getProductTypes();
        category = categoryService.findCategory("Clothing");
        List<ProductType> clothes = category.getProductTypes();
        category = categoryService.findCategory("Accessories");
        List<ProductType> accessories = category.getProductTypes();
        model.addAttribute("Shoes", shoes);
        model.addAttribute("Clothing", clothes);
        model.addAttribute("Accessories", accessories);
    }
}
