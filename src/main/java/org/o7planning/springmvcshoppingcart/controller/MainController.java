package org.o7planning.springmvcshoppingcart.controller;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.entity.*;
import org.o7planning.springmvcshoppingcart.model.*;
import org.o7planning.springmvcshoppingcart.service.*;
import org.o7planning.springmvcshoppingcart.validator.ChangePasswordValidator;
import org.o7planning.springmvcshoppingcart.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.o7planning.springmvcshoppingcart.validator.CustomerInfoValidator;

/**
 * Main controller
 */
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class MainController {

    private static final Logger log = Logger.getLogger(MainController.class);

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerInfoValidator customerInfoValidator;

    @Autowired
    private RegistrationValidator registrationValidator;

    @Autowired
    private ChangePasswordValidator changePasswordValidator;

    /**
     * Register validators
     */
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        log.info("Target: "+target);

        if (customerInfoValidator.supports(target.getClass())) {
            dataBinder.setValidator(customerInfoValidator);
        }
        if (registrationValidator.supports(target.getClass())) {
            dataBinder.setValidator(registrationValidator);
        }
        if (changePasswordValidator.supports(target.getClass())) {
            dataBinder.setValidator(changePasswordValidator);
        }
    }

    /**
     * Access denied page
     * @return
     */
    @RequestMapping("/403")
    public String accessDenied() {
        return "/403";
    }

    /**
     * Home page
     * @return
     */
    @RequestMapping(value = {"/index","/" })
    public String index() {
        return "/index";
    }

    /**
     * Registration page
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationForm( Model model) {
        AccountInfo accountInfo = new AccountInfo();
        model.addAttribute("accountInfo", accountInfo);
        return "registration";
    }

    /**
     * Save new account
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveUser(@Validated AccountInfo accountInfo, BindingResult result){
        if (result.hasErrors()) {
            log.info("Validate error");
            return "registration";
        }
        accountService.saveAccount(accountInfo);
        return "registrationSuccess";

    }

    /**
     * Show login page
     */
    @RequestMapping(value = { "/login" }, method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    /**
     * Show account information page
     */
    @RequestMapping(value = { "/accountInfo" }, method = RequestMethod.GET)
    public String accountInfo(Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountService.findAccount(userDetails.getUsername());
        model.addAttribute("account", account);
        return "accountInfo";
    }

    /**
     * Page for change account information
     */
    @RequestMapping(value = { "/changeAccountInfo" }, method = RequestMethod.GET)
    public String changeAccountInfo(Model model) {
        log.debug("Change account information");
        CustomerInfo customerInfo = new CustomerInfo();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountService.findAccount(userDetails.getUsername());
        model.addAttribute("customerForm", customerInfo);
        model.addAttribute("account", account);
        return "changeAccountInfo";
    }

    /**
     * Save changes
     */
    @RequestMapping(value = { "/changeAccountInfo" }, method = RequestMethod.POST)
    public String changeAccountInfoSave(@ModelAttribute("customerForm") @Validated CustomerInfo customerInfo, BindingResult result) {

        if (result.hasErrors()) {
            customerInfo.setValid(false);
            log.info("Validate error");
            return "changeAccountInfo";
        }
        accountService.changeAccountInfo(customerInfo);
        return "redirect:/accountInfo";
    }

    /**
     * Change password page
     */
    @RequestMapping(value = { "/changePassword" }, method = RequestMethod.GET)
    public String changePassword(Model model) {
        log.debug("Change password");
        ChangePasswordInfo changePasswordInfo = new ChangePasswordInfo();
        model.addAttribute("changePasswordInfo", changePasswordInfo);
        return "changePassword";
    }

    /**
     * Save new password
     */
    @RequestMapping(value = { "/changePassword" }, method = RequestMethod.POST)
    public String saveChangePassword(@Validated ChangePasswordInfo changePasswordInfo, BindingResult result) {
        if (result.hasErrors()) {
            log.info("Validate error");
            return "changePassword";
        }
        accountService.saveAccount(changePasswordInfo);
        return "redirect:/accountInfo";
    }

}