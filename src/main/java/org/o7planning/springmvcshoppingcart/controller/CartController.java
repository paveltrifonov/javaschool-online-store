package org.o7planning.springmvcshoppingcart.controller;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.exceptions.EmptyCartException;
import org.o7planning.springmvcshoppingcart.exceptions.NotEnoughProductsException;
import org.o7planning.springmvcshoppingcart.exceptions.NotValidCustomerException;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.CartInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;
import org.o7planning.springmvcshoppingcart.service.*;
import org.o7planning.springmvcshoppingcart.util.Utils;
import org.o7planning.springmvcshoppingcart.validator.CustomerInfoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Cart controller
 */
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class CartController {

    private static final Logger log = Logger.getLogger(CartController.class);

    @Autowired
    private CustomerInfoValidator customerInfoValidator;

    @Autowired
    private ProductService productService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private EmailService emailService;

    /**
     * Register validators
     */
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        log.info("Target: "+target);

        if (customerInfoValidator.supports(target.getClass())) {
            dataBinder.setValidator(customerInfoValidator);
        }
    }

    /**
     * Delete product from cart
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping({ "/shoppingCartRemoveProduct" })
    public String removeProductHandler(HttpServletRequest request,
                                       @RequestParam(value = "code", defaultValue = "") String code) throws ProductNotFoundException {

        log.debug("Delete product from cart");
        Product product = null;
        if (code != null && code.length() > 0) {
            product = productService.findProduct(code);
        }
        if (product != null) {
            // Cart Info stored in Session.
            CartInfo cartInfo = Utils.getCartInSession(request);
            ProductInfo productInfo = new ProductInfo(product);
            cartInfo.removeProduct(productInfo);
        }
        // Redirect to shoppingCart page.
        return "redirect:/shoppingCart";
    }

    /**
     * Show cart
     */
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
    public String shoppingCartHandler(HttpServletRequest request, Model model) {
        CartInfo myCart = Utils.getCartInSession(request);
        model.addAttribute("cartForm", myCart);
        return "shoppingCart";
    }

    /**
     * Update quantity of products in cart.
     */
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
    public String shoppingCartUpdateQty(HttpServletRequest request,
                                        @ModelAttribute("cartForm") CartInfo cartForm) {

        log.debug("Update quantity of products in cart");
        CartInfo cartInfo = Utils.getCartInSession(request);
        cartInfo.updateQuantity(cartForm);
        return "redirect:/shoppingCart";
    }

    /**
     * Enter customer information.
     * @throws EmptyCartException if cart is empty
     */
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
    public String shoppingCartCustomerForm(HttpServletRequest request, Model model) throws EmptyCartException {

        CartInfo cartInfo = Utils.getCartInSession(request);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountService.findAccount(userDetails.getUsername());
        if (cartInfo.isEmpty()) {
            log.info("Cart is empty");
            throw new EmptyCartException();
        }
        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        if (customerInfo == null) {
            customerInfo = new CustomerInfo();
        }
        model.addAttribute("customerForm", customerInfo);
        model.addAttribute("account", account);
        return "shoppingCartCustomer";
    }

    /**
     * Save customer information.
     */
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
    public String shoppingCartCustomerSave(HttpServletRequest request,
                                           @ModelAttribute("customerForm") @Validated CustomerInfo customerForm,
                                           BindingResult result) {

        if (result.hasErrors()) {
            customerForm.setValid(false);
            log.info("Validate error");
            return "shoppingCartCustomer";
        }
        customerForm.setValid(true);
        CartInfo cartInfo = Utils.getCartInSession(request);
        cartInfo.setCustomerInfo(customerForm);
        return "redirect:/shoppingCartConfirmation";
    }

    /**
     * Review Cart to confirm.
     * @throws EmptyCartException if cart is empty
     * @throws NotValidCustomerException if customer information is not valid
     */
    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
    public String shoppingCartConfirmationReview(HttpServletRequest request) throws EmptyCartException, NotValidCustomerException {
        CartInfo cartInfo = Utils.getCartInSession(request);
        if (cartInfo.isEmpty()) {
            log.debug("Cart is empty");
            throw new EmptyCartException();
        }
        if (!cartInfo.isValidCustomer()) {
            log.warn("Not valid customer");
            throw new NotValidCustomerException();
        }
        return "shoppingCartConfirmation";
    }

    /**
     * Send Cart (Save).
     * @throws ProductNotFoundException if product not found
     * @throws EmptyCartException if cart is empty
     * @throws NotValidCustomerException if customer information is not valid
     */
    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)
    public String shoppingCartConfirmationSave(HttpServletRequest request) throws ProductNotFoundException, EmptyCartException, NotValidCustomerException {
        log.debug("Send cart");
        CartInfo cartInfo = Utils.getCartInSession(request);
        if (cartInfo.isEmpty()) {
            log.debug("Cart is empty");
            throw new EmptyCartException();
        }
        if (!cartInfo.isValidCustomer()) {
            log.warn("Not valid customer");
            throw new NotValidCustomerException();
        }
        try {
            productService.checkCart(cartInfo.getCartLines());
            orderService.saveOrder(cartInfo);
            emailService.sendMail(cartInfo.getCustomerInfo().getEmail(), cartInfo.getOrderNum());
        }
        catch (NotEnoughProductsException e){
            log.error(e.getMessage());
            Utils.removeCartInSession(request);
            return "buyError";
        }
        catch (MailSendException e){
            log.error(e.getMessage());
        }
        // Remove Cart In Session.
        Utils.removeCartInSession(request);
        // Store Last ordered cart to Session.
        Utils.storeLastOrderedCartInSession(request, cartInfo);
        return "redirect:/shoppingCartFinalize";
    }

    /**
     * Successful order page
     */
    @RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
    public String shoppingCartFinalize(HttpServletRequest request) {

        CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);
        if (lastOrderedCart == null) {
            return "redirect:/shoppingCart";
        }
        return "shoppingCartFinalize";
    }

    /**
     * Error page if order is unsuccessful
     */
    @RequestMapping(value = { "/buyError" }, method = RequestMethod.GET)
    public String buyError(HttpServletRequest request) {

        CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);
        if (lastOrderedCart == null) {
            return "redirect:/shoppingCart";
        }
        return "buyError";
    }
}
