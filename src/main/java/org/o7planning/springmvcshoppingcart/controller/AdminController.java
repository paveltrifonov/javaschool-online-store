package org.o7planning.springmvcshoppingcart.controller;

import java.util.*;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.entity.*;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.*;
import org.o7planning.springmvcshoppingcart.service.*;
import org.o7planning.springmvcshoppingcart.validator.AttributeInfoValidator;
import org.o7planning.springmvcshoppingcart.validator.CategoryInfoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Admin controller
 */
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class AdminController {

    private static final Logger log = Logger.getLogger(AdminController.class);

    @Autowired
    MessageSender messageSender;

    @Autowired
    private ProductService productService;

    @Autowired
    private AttributeValueService attributeValueService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private AttributeInfoValidator attributeInfoValidator;

    @Autowired
    private CategoryInfoValidator categoryInfoValidator;

    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private AttributeService attributeService;

    @Autowired
    private CategoryService categoryService;

    /**
     * Register validators
     */
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        log.info("Target: "+target);

        if(attributeInfoValidator.supports(target.getClass())){
            dataBinder.setValidator(attributeInfoValidator);
        }
        if(categoryInfoValidator.supports(target.getClass())){
            dataBinder.setValidator(categoryInfoValidator);
        }
    }

    /**
     * Page for select category for new product
     */
    @RequestMapping(value = { "/productCategory" }, method = RequestMethod.GET)
    public String productCategory(Model model) {
        log.debug("Select category");
        ProductInfo productInfo = new ProductInfo();
        LinkedHashSet<Category> set = new LinkedHashSet<Category>(categoryService.findAll());
        model.addAttribute("productForm", productInfo);
        model.addAttribute("categoriesList", set);
        return "productCategory";
    }

    /**
     * Sending selected category and redirect to subcategory page
     * @param productInfo product information
     * @param redirectAttributes for send selected category to the next page
     */
    @RequestMapping(value = { "/productCategory" }, method = RequestMethod.POST)
    public String productCategorySave(@ModelAttribute("productForm")  ProductInfo productInfo,
                                      RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("productForm", productInfo);
        return "redirect:productSubCategory";
    }

    /**
     * Page for select subcategory for new product
     * @param productInfo product information
     */
    @RequestMapping(value = { "/productSubCategory" }, method = RequestMethod.GET)
    public String productSubCategory(Model model,  @ModelAttribute("productForm")  ProductInfo productInfo) {
        log.debug("Select subcategory");
        Category category = categoryService.findCategory(productInfo.getCategory());
        List<ProductType> set = category.getProductTypes();
        model.addAttribute("productForm", productInfo);
        model.addAttribute("productTypeList", set);
        return "productSubCategory";
    }

    /**
     * Sending selected category and subcategory for new product to the product create page
     * @param productInfo product information
     * @param redirectAttributes for send selected subcategory to the next page
     */
    @RequestMapping(value = { "/productSubCategory" }, method = RequestMethod.POST)
    public String productSubCategorySave(@ModelAttribute("productForm")  ProductInfo productInfo,
                                      RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("productForm", productInfo);
        return "redirect:product";
    }

    /**
     * Page for change orders status
     * @param orderId order id
     * @throws OrderNotFoundException if order not found
     */
    @RequestMapping(value = { "/changeStatus" }, method = RequestMethod.GET)
    public String selectStatus(Model model, @RequestParam("orderId") String orderId) throws OrderNotFoundException {
        log.debug("Change order status");
        OrderInfo orderInfo = orderService.getOrderInfo(orderId);
        model.addAttribute("orderInfo", orderInfo);
        return "changeStatus";
    }

    /**
     * Update order status
     */
    @RequestMapping(value = { "/changeStatus" }, method = RequestMethod.POST)
    public String changeStatus(@ModelAttribute ("orderInfo") OrderInfo orderInfo) {

       orderService.updateStatus(orderInfo.getId(), orderInfo.getStatus());
        return "redirect:/orderList";
    }

    /**
     * Page for create new subcategory
     */
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String category( Model model) {
        log.debug("Create new subcategory");
        LinkedHashSet<Category> categories = categoryService.findAll();
        LinkedHashSet<Attribute> set = attributeService.findAll();
        LinkedHashSet<ProductType> productTypes = productTypeService.findAll();
        model.addAttribute("categoryInfo", new CategoryInfo());
        model.addAttribute("attributeList", set);
        model.addAttribute("categoryList", categories);
        model.addAttribute("subcategoryList", productTypes);
        return "category";
    }

    /**
     * Save new subcategory
     * @param categoryInfo subcategory information
     * @param result contains information about validator errors
     */
    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public String saveCategory(@Validated CategoryInfo categoryInfo, BindingResult result, Model model){

        if (result.hasErrors()) {
            log.info("Validate error");
            return "category";
        }

        ProductTypeInfo productTypeInfo = new ProductTypeInfo();
        productTypeInfo.setTypeName(categoryInfo.getTypeName());
        productTypeInfo.setCategory(categoryInfo.getCategory());
        LinkedHashSet<Attribute> set = new LinkedHashSet<Attribute>();
        for (String attribute: categoryInfo.getAttributes()) {
            set.add(attributeService.findAttribute(attribute));
        }
        productTypeInfo.setAttributes(set);
        productTypeService.saveProductType(productTypeInfo);
        return "categorySuccess";

    }

    /**
     * Page for create new category
     */
    @RequestMapping(value = "/attribute", method = RequestMethod.GET)
    public String attribute( Model model) {
        log.debug("Create new attribute");
        AttributeInfo attributeInfo = new AttributeInfo();
        LinkedHashSet<Attribute> attributes = attributeService.findAll();
        model.addAttribute("attributeList", attributes);
        model.addAttribute("attributesForm", attributeInfo);
        return "attribute";
    }

    /**
     * Save new attribute
     * @param attributeInfo attribute information
     * @param result contains information about validator errors
     * @return
     */
    @RequestMapping(value = "/attribute", method = RequestMethod.POST)
    public String saveAttribute(@Validated @ModelAttribute("attributesForm") AttributeInfo attributeInfo, BindingResult result, Model model){

        if (result.hasErrors()) {
            log.info("Validate error");
            LinkedHashSet<Attribute> attributes = attributeService.findAll();
            model.addAttribute("attributeList", attributes);
            return "attribute";
        }
        attributeService.saveAttribute(attributeInfo);
        return "attributeSuccess";
    }

    /**
     * Page for administer the advertising banner
     */
    @RequestMapping(value = { "/banner" }, method = RequestMethod.GET)
    public String productsForBanner(Model model, //
                                     @RequestParam(value = "name", defaultValue = "") String likeName,
                                     @RequestParam(value = "page", defaultValue = "1") int page) {
        final int maxResult = 12;
        final int maxNavigationPage = 10;
        messageSender.sendMessage();
        PaginationResult<ProductInfo> result = productService.queryProducts(page, maxResult, maxNavigationPage, likeName);
        List<AttributeWithValueInfo> attributes = attributeValueService.getAttributes(result.getList());
        LinkedHashSet<ProductType> set = new LinkedHashSet<ProductType>(productTypeService.findAll());
        model.addAttribute("productTypeList", set);
        model.addAttribute("attributes", attributes);
        model.addAttribute("paginationProducts", result);
        model.addAttribute("bannerForm", new BannerListInfo());
        return "banner";
    }

    /**
     * Save changes for banner
     */
    @RequestMapping(value = { "/banner" }, method = RequestMethod.POST)
    public String updateProductsForBanner(@ModelAttribute("bannerForm") BannerListInfo bannerListInfo, HttpServletRequest request) {

        for(BannerInfo bannerInfo: bannerListInfo.getBannerInfos()){
            ProductInfo productInfo = productService.findProductInfo(bannerInfo.getCode());
            if(productInfo.isBanner() != bannerInfo.isBanner()) {
                productInfo.setBanner(bannerInfo.isBanner());
                productService.save(productInfo);
            }
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

}