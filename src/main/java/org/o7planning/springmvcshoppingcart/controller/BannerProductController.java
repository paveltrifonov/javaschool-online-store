package org.o7planning.springmvcshoppingcart.controller;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.model.BannerProduct;
import org.o7planning.springmvcshoppingcart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Send items for inclusion on advertising stand
 */
@RestController
public class BannerProductController {

    private static final Logger log = Logger.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/api/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List<BannerProduct> getProducts() {
        log.info("Sending banner products");
        List<BannerProduct> bannerProducts = productService.getBannerProducts();
        return bannerProducts;
    }

}
