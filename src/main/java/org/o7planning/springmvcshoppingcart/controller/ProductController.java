package org.o7planning.springmvcshoppingcart.controller;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.entity.*;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.*;
import org.o7planning.springmvcshoppingcart.service.AttributeValueService;
import org.o7planning.springmvcshoppingcart.service.ProductService;
import org.o7planning.springmvcshoppingcart.service.ProductTypeService;
import org.o7planning.springmvcshoppingcart.util.Utils;
import org.o7planning.springmvcshoppingcart.validator.ProductInfoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Product controller
 */
@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class ProductController {

    private static final Logger log = Logger.getLogger(ProductController.class);

    @Autowired
    private ProductInfoValidator productInfoValidator;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private AttributeValueService attributeValueService;

    /**
     * Register validators
     */
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        log.info("Target: "+target);
        if (productInfoValidator.supports(target.getClass())) {
            dataBinder.setValidator(productInfoValidator);
            // For upload Image.
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    /**
     * Show create product page
     */
    @RequestMapping(value = { "/product" }, method = RequestMethod.GET)
    public String product(Model model, @RequestParam(value = "code", defaultValue = "") String code, @ModelAttribute("productForm") ProductInfo productInfo) {

        if (code != null && code.length() > 0) {
            log.debug("Product with code: " + code);
            productInfo = productService.findProductInfo(code);
        }
        if (productInfo == null) {
            log.debug("New product");
            productInfo = new ProductInfo();
            productInfo.setNewProduct(true);
        }
        LinkedHashSet<Attribute> attributes = new LinkedHashSet<Attribute>(productTypeService.getCategoryAttributes(productInfo.getProductType()));
        model.addAttribute("attributeList", attributes);
        return "product";
    }

    /**
     * Save product
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping(value = { "/product" }, method = RequestMethod.POST)
    public String productSave(@ModelAttribute("productForm") @Validated ProductInfo productInfo,
                              BindingResult result) throws ProductNotFoundException {

        if (result.hasErrors()) {
            log.debug("Validator error");
            return "product";
        }
            log.debug("Save new product");
            LinkedHashSet<Attribute> attributes = productTypeService.getCategoryAttributes(productInfo.getProductType());
            AttributeValueInfo attributeValueInfo = new AttributeValueInfo();
            ArrayList<String> arrayList = productInfo.getAtrValue();
            int i = 0;
            productInfo.setNewProduct(true);
            productService.save(productInfo);
            for (Attribute atr : attributes) {
                attributeValueInfo.setAttributeValueName(arrayList.get(i));
                attributeValueInfo.setAttribute(atr);
                attributeValueInfo.setProduct(productService.findProduct(productInfo.getCode()));
                attributeValueService.saveAttributeValue(attributeValueInfo);
                i++;
            }
            return "redirect:productList";

    }

    /**
     * Edit product page
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping(value = { "/editProduct" }, method = RequestMethod.GET)
    public String productEdit(Model model, @RequestParam(value = "code", defaultValue = "") String code,
                              @ModelAttribute("productForm")  ProductInfo productInfo) throws ProductNotFoundException {

        log.info("Edit product");
        Product product = productService.findProduct(code);
        productInfo.setName(product.getName());
        productInfo.setPrice(product.getPrice());
        productInfo.setProductType(product.getProductType().getTypeName());
        productInfo.setStorage(product.getStorage());
        LinkedHashSet<Attribute> attributes = new LinkedHashSet<Attribute>(productTypeService.getCategoryAttributes(productInfo.getProductType()));
        LinkedHashSet<AttributeValue> attributeValues = attributeValueService.findAll();
        ArrayList<String> arrayList = new ArrayList<String>();
        for(AttributeValue attributeValue: attributeValues){
            if (attributeValue.getProduct().getCode().equals(productInfo.getCode())){
                arrayList.add(attributeValue.getAttributeValueName());
            }
        }
        model.addAttribute("arrayList", arrayList);
        model.addAttribute("attributeList", attributes);
        return "editProduct";
    }

    /**
     * Save changes
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping(value = { "/editProduct" }, method = RequestMethod.POST)
    public String productSaveEdit(@ModelAttribute("productForm") @Validated ProductInfo productInfo, BindingResult result) throws ProductNotFoundException {

        if (result.hasErrors()) {
            log.debug("Validate error");
            return "editProduct";
        }
            log.debug("Update product");
            productService.save(productInfo);
            attributeValueService.saveNewAttributeValue(productInfo);
        return "redirect:productList";
    }

    /**
     * Product list page
     */
    @RequestMapping({ "/productList" })
    public String listProductHandler(Model model, //
                                     @RequestParam(value = "name", defaultValue = "") String likeName,
                                     @RequestParam(value = "page", defaultValue = "1") int page) {
        final int maxResult = 12;
        final int maxNavigationPage = 10;

        PaginationResult<ProductInfo> result = productService.queryProducts(page, maxResult, maxNavigationPage, likeName);
        List<AttributeWithValueInfo> attributes = attributeValueService.getAttributes(result.getList());
        LinkedHashSet<ProductType> set = new LinkedHashSet<ProductType>(productTypeService.findAll());
        model.addAttribute("productTypeList", set);
        model.addAttribute("attributes", attributes);
        model.addAttribute("paginationProducts", result);
        return "productList";
    }

    /**
     * List of products by subcategory
     */
    @RequestMapping( value ={ "/productListCategory" },  method = RequestMethod.GET)
    public String listProductCategory(Model model,
                                      @RequestParam(value = "category", defaultValue = "") String category) {

        log.info("Products in category "+ category);
        List<ProductInfo> result = productService.getProductsBySubcategory(category);
        ProductInfo filterProduct = new ProductInfo();
        filterProduct.setProductType(category);
        List<AttributeWithValueInfo> attributes = attributeValueService.getAttributes(result);
        List<AttributeInfo> attributeInfos = productTypeService.getFilteredValues(category);
        model.addAttribute("filterAttrValues",attributeInfos);
        model.addAttribute("attributes", attributes);
        model.addAttribute("productsInCategory", result);
        model.addAttribute("category", category);
        model.addAttribute("filterProduct", filterProduct);
        return "productListCategory";
    }

    /**
     * Filter products by selected parameters
     * @param filterProduct product information for filter products
     * @return filtered products page
     */
    @RequestMapping( value ={ "/productListCategory" },  method = RequestMethod.POST)
    public String filterListProductCategory(  @ModelAttribute("filterProduct") ProductInfo filterProduct,
                                              RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("filterProduct", filterProduct);
        return "redirect:filterProduct";
    }

    /**
     * Filtered products page
     * @param filterProduct product information for filter products
     */
    @RequestMapping( value ={ "/filterProduct" }, method = RequestMethod.GET)
    public String filterProduct( Model model,
                                 @ModelAttribute("filterProduct")  ProductInfo filterProduct){

        List<ProductInfo> result = productService.getProductsBySubcategory(filterProduct.getCategory());
        List<AttributeWithValueInfo> attributes = attributeValueService.getAttributes(result);
        List<AttributeInfo> attributeInfos = productTypeService.getFilteredValues(filterProduct.getCategory());
        List<ProductInfo> trueProducts = productService.filterProducts(result, attributes, filterProduct);
        BigDecimal filterPrice;
        if(filterProduct.getName().equals("")){
            filterPrice=new BigDecimal(0);
        } else{
            filterPrice= BigDecimal.valueOf(Double.parseDouble(filterProduct.getName()));
        }
        model.addAttribute("filterAttrValues",attributeInfos);
        model.addAttribute("filterPrice",filterPrice);
        model.addAttribute("attributes", attributes);
        model.addAttribute("trueProducts", trueProducts);
        return "filterProduct";
    }

    /**
     * Get products with selected parameters
     * @param filterProduct product information for filter products
     */
    @RequestMapping( value ={ "/filterProduct" },  method = RequestMethod.POST)
    public String repeatFilterProduct(  @ModelAttribute("filterProduct") ProductInfo filterProduct,
                                        RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("filterProduct", filterProduct);
        return "redirect:filterProduct";
    }

    /**
     * Put product in the cart
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping({ "/buyProduct" })
    public String listProductHandler(HttpServletRequest request,
                                     @RequestParam(value = "code", defaultValue = "") String code) throws ProductNotFoundException {

        Product product = null;
        if (code != null && code.length() > 0) {
            product = productService.findProduct(code);
        }
        if (product != null) {
            // Cart info stored in Session.
            CartInfo cartInfo = Utils.getCartInSession(request);
            ProductInfo productInfo = new ProductInfo(product);
            cartInfo.addProduct(productInfo, 1);
        }
        return "redirect:/shoppingCart";
    }

    /**
     * Get product image
     * @param code product code
     * @throws ProductNotFoundException if product not found
     */
    @RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
    public void productImage(HttpServletResponse response,
                             @RequestParam("code") String code) throws IOException, ProductNotFoundException {
        Product product = null;
        if (code != null) {
            product = this.productService.findProduct(code);
        }
        if (product != null && product.getImage() != null) {
            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(product.getImage());
        }
        response.getOutputStream().close();
    }
}
