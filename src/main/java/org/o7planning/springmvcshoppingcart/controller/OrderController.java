package org.o7planning.springmvcshoppingcart.controller;

import org.apache.log4j.Logger;
import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.Order;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.OrderDetailInfo;
import org.o7planning.springmvcshoppingcart.model.OrderInfo;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.o7planning.springmvcshoppingcart.service.AccountService;
import org.o7planning.springmvcshoppingcart.service.CategoryService;
import org.o7planning.springmvcshoppingcart.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Controller
// Enable Hibernate Transaction.
@Transactional
// Need to use RedirectAttributes
@EnableWebMvc
public class OrderController {

    private static final Logger log = Logger.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private AccountService accountService;

    /**
     * Order Page
     * @throws OrderNotFoundException if order not found
     */
    @RequestMapping(value = { "/order" }, method = RequestMethod.GET)
    public String orderView(Model model, @RequestParam("orderId") String orderId) throws OrderNotFoundException {
        OrderInfo orderInfo = null;
        if (orderId != null) {
            orderInfo = orderService.getOrderInfo(orderId);
        }
        if (orderInfo == null) {
            log.error("Order with id" + orderId + "not found");
            throw new OrderNotFoundException();
        }
        List<OrderDetailInfo> details = orderService.listOrderDetailInfos(orderId);
        orderInfo.setDetails(details);
        model.addAttribute("orderInfo", orderInfo);
        return "order";
    }

    /**
     * Order list page
     */
    @RequestMapping(value = { "/orderList" }, method = RequestMethod.GET)
    public String orderList(Model model,
                            @RequestParam(value = "page", defaultValue = "1") String pageStr) {
        int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (NumberFormatException e) {
            log.error("Wrong page number");
        }
        final int MAX_RESULT = 10;
        final int MAX_NAVIGATION_PAGE = 10;

        PaginationResult<OrderInfo> paginationResult //
                = orderService.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
        model.addAttribute("paginationResult", paginationResult);
        return "orderList";
    }

    /**
     * List of user orders page
     */
    @RequestMapping(value = { "/orderUserList" }, method = RequestMethod.GET)
    public String orderUserList(Model model) {
        Account account = accountService.findAccount(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Order> orders = account.getOrders();
        model.addAttribute("orders", orders);
        return "orderUserList";
    }
}
