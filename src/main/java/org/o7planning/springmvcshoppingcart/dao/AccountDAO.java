package org.o7planning.springmvcshoppingcart.dao;

import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.model.AccountInfo;
import org.o7planning.springmvcshoppingcart.model.ChangePasswordInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;

/**
 * Dao user account
 */

public interface AccountDAO {

    /**
     * Find account by user name( used to login)
     * @param userName user name for find account
     * @return account
     */
    Account findAccount(String userName );

    /**
     * Update old or save new account
     * @param accountInfo information to save or update account
     */
    void saveAccount(AccountInfo accountInfo);

    /**
     * Change password
     * @param changePasswordInfo information to change password
     */
    void saveAccount(ChangePasswordInfo changePasswordInfo);

    /**
     * Change customer info
     * @param customerInfo information to change customer info
     */
    void changeAccountInfo(CustomerInfo customerInfo);

}