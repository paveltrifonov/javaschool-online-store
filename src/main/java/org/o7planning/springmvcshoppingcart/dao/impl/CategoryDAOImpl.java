package org.o7planning.springmvcshoppingcart.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.o7planning.springmvcshoppingcart.dao.CategoryDAO;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.LinkedHashSet;

/**
 * CategoryDAO implementation
 */
@Transactional
@Repository("categoryDAO")
public class CategoryDAOImpl implements CategoryDAO {

    private static final Logger log = Logger.getLogger(CategoryDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Category findCategory(String categoryName) {
        log.debug("Find category with name: " + categoryName);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findCategory").setString("categoryName", categoryName);
        return (Category) query.uniqueResult();
    }

    @Override
    public LinkedHashSet<Category> findAll() {
        log.debug("Get all categories");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAllCategory");
        LinkedHashSet<Category> categories = new LinkedHashSet<Category>(query.list());
        return categories;
    }

    @Override
    public LinkedHashSet<ProductType> getProductTypes(String categoryName) {
        log.debug("Get all subcategories of category:" + categoryName);
        Session session = sessionFactory.getCurrentSession();
        Category category = findCategory(categoryName);
        LinkedHashSet<ProductType> productTypes = new LinkedHashSet<ProductType>(category.getProductTypes());
        return productTypes;
    }

    @Override
    public void saveCategory(Category category) {
        log.info("Save new category");
        sessionFactory.getCurrentSession().persist(category);
    }
}
