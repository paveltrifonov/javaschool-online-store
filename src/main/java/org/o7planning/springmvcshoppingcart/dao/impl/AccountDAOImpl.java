package org.o7planning.springmvcshoppingcart.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.o7planning.springmvcshoppingcart.dao.AccountDAO;
import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.model.AccountInfo;
import org.o7planning.springmvcshoppingcart.model.ChangePasswordInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.apache.log4j.Logger;

/**
 * AccountDAO implementation
 */

// Transactional for Hibernate
@Transactional
@Repository("accountDAO")
public class AccountDAOImpl implements AccountDAO {

    private static final Logger log = Logger.getLogger(AccountDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Account findAccount(String userName ) {
        log.debug("Find account by name: " + userName);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAccount").setString("userName", userName);
        return (Account) query.uniqueResult();
    }


    public void saveAccount(AccountInfo accountInfo){
        log.info("Save new account with name: " + accountInfo.getUserName());
        Session session = sessionFactory.getCurrentSession();
        Account account = new Account();
        account.setActive(true);
        account.setUserRole("USER");
        account.setPassword(accountInfo.getPassword());
        account.setUserName(accountInfo.getUserName());
        account.setName(accountInfo.getName());
        account.setAddress(accountInfo.getAddress());
        account.setEmail(accountInfo.getEmail());
        account.setPhone(accountInfo.getPhone());
        session.persist(account);
    }

    public void saveAccount(ChangePasswordInfo changePasswordInfo){
        log.info("Change account password");
        Session session = sessionFactory.getCurrentSession();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = findAccount(userDetails.getUsername());
        account.setPassword(changePasswordInfo.getNewPassword());
        session.persist(account);

    }

    @Override
    public void changeAccountInfo(CustomerInfo customerInfo) {
        log.info("Change account information");
        Session session = sessionFactory.getCurrentSession();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = findAccount(userDetails.getUsername());
        account.setName(customerInfo.getName());
        account.setEmail(customerInfo.getEmail());
        account.setPhone(customerInfo.getPhone());
        account.setAddress(customerInfo.getAddress());
        session.persist(account);
    }
}