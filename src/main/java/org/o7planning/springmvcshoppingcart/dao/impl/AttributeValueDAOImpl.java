package org.o7planning.springmvcshoppingcart.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.o7planning.springmvcshoppingcart.dao.AttributeValueDAO;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.model.AttributeValueInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.LinkedHashSet;

/**
 * AttributeValueDAO implementation
 */
@Transactional
@Repository("attributeValueDAO")
public class AttributeValueDAOImpl implements AttributeValueDAO {

    private static final Logger log = Logger.getLogger(AttributeValueDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AttributeValue findAttributeValue(Integer id) {
        log.debug("Find attribute value by id: " + id);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAttributeValue").setParameter("id", id);
        return (AttributeValue) query.uniqueResult();
    }

    @Override
    public LinkedHashSet<AttributeValue> findAll() {
        log.debug("Get all attribute values");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAllValues");
        LinkedHashSet<AttributeValue> attributeValues = new LinkedHashSet<AttributeValue>(query.list());
        return attributeValues;
    }

    @Override
    public void saveAttributeValue(AttributeValueInfo attributeValueInfo) {
        Integer id = attributeValueInfo.getId();
        AttributeValue attributeValue = null;

        boolean isNew = false;
        if (id != null) {
            log.info("Update value of attribute: " + attributeValueInfo.getAttribute());
            attributeValue = this.findAttributeValue(id);
        }
        if (attributeValue == null) {
            isNew = true;
            log.info("Save new value of attribute: " + attributeValueInfo.getAttribute());
            attributeValue = new AttributeValue();
        }

        attributeValue.setAttributeValueName(attributeValueInfo.getAttributeValueName());
        attributeValue.setProduct(attributeValueInfo.getProduct());
        attributeValue.setAttribute(attributeValueInfo.getAttribute());

        if (isNew) {
            this.sessionFactory.getCurrentSession().persist(attributeValue);
        }

        this.sessionFactory.getCurrentSession().flush();
    }
}
