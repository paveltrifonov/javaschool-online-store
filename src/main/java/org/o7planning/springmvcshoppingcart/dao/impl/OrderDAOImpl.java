package org.o7planning.springmvcshoppingcart.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.o7planning.springmvcshoppingcart.dao.AccountDAO;
import org.o7planning.springmvcshoppingcart.dao.OrderDAO;
import org.o7planning.springmvcshoppingcart.dao.ProductDAO;
import org.o7planning.springmvcshoppingcart.entity.Account;
import org.o7planning.springmvcshoppingcart.entity.Order;
import org.o7planning.springmvcshoppingcart.entity.OrderDetail;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.CartInfo;
import org.o7planning.springmvcshoppingcart.model.CartLineInfo;
import org.o7planning.springmvcshoppingcart.model.CustomerInfo;
import org.o7planning.springmvcshoppingcart.model.OrderDetailInfo;
import org.o7planning.springmvcshoppingcart.model.OrderInfo;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * OrderDAO implementation
 */
//Transactional for Hibernate
@Transactional
@Repository("orderDAO")
public class OrderDAOImpl implements OrderDAO {

    private static final Logger log = Logger.getLogger(OrderDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private AccountDAO accountDAO;

    private int getMaxOrderNum() {
        log.debug("Get last order num");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("maxOrderNum");
        Integer value = (Integer) query.uniqueResult();
        if (value == null) {
            return 0;
        }
        return value;
    }
    @Override
    public void updateStatus(String id, String status){
        log.info("Update order status to: " + status);
        Session session = sessionFactory.getCurrentSession();
        Order updatedOrder = findOrder(id);
        updatedOrder.setStatus(status);
        session.persist(updatedOrder);
    }

    @Override
    public void saveOrder(CartInfo cartInfo) {
        Session session = sessionFactory.getCurrentSession();

        int orderNum = this.getMaxOrderNum() + 1;
        log.info("Save new order with id: " + orderNum);
        Order order = new Order();

        order.setId(UUID.randomUUID().toString());
        order.setOrderNum(orderNum);
        order.setOrderDate(new Date());
        order.setAmount(cartInfo.getAmountTotal());
        order.setStatus("Paid");

        Account account = accountDAO.findAccount(SecurityContextHolder.getContext().getAuthentication().getName());
        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        order.setCustomerName(customerInfo.getName());
        order.setCustomerEmail(customerInfo.getEmail());
        order.setCustomerPhone(customerInfo.getPhone());
        order.setCustomerAddress(customerInfo.getAddress());
        order.setAccount(account);
        session.persist(order);

        List<CartLineInfo> lines = cartInfo.getCartLines();

        for (CartLineInfo line : lines) {
            OrderDetail detail = new OrderDetail();
            detail.setId(UUID.randomUUID().toString());
            detail.setOrder(order);
            detail.setAmount(line.getAmount());
            detail.setPrice(line.getProductInfo().getPrice());
            detail.setQuanity(line.getQuantity());
            String code = line.getProductInfo().getCode();
            Product product = this.productDAO.findProduct(code);
            detail.setProduct(product);
            session.persist(detail);
        }
        // Set OrderNum for report.
        cartInfo.setOrderNum(orderNum);
    }

    // @page = 1, 2, ...
    @Override
    public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage) {
        log.debug("Get order list");
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("orders");
        return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public Order findOrder(String orderId) {
        log.debug("Find order by id: " + orderId);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findOrder").setString("orderId", orderId);
        return (Order) query.uniqueResult();
    }

    @Override
    public OrderInfo getOrderInfo(String orderId) throws OrderNotFoundException {
        Order order = this.findOrder(orderId);
        if (order == null) {
            log.error("Order with id " + orderId +" not found");
            throw new OrderNotFoundException("Order with id" + orderId + " not found");
        }
        return new OrderInfo(order.getId(), order.getOrderDate(), //
                order.getOrderNum(), order.getAmount(), order.getCustomerName(), //
                order.getCustomerAddress(), order.getCustomerEmail(), order.getCustomerPhone(), order.getStatus());
    }

    @Override
    public List<OrderDetailInfo> listOrderDetailInfos(String orderId) {
        log.debug("Get detail information of order with id: " + orderId);
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("orderDetails").setString("orderId", orderId);

        return query.list();
    }

}