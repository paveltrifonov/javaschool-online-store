package org.o7planning.springmvcshoppingcart.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.o7planning.springmvcshoppingcart.dao.CategoryDAO;
import org.o7planning.springmvcshoppingcart.dao.ProductTypeDAO;
import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;
import org.o7planning.springmvcshoppingcart.model.ProductTypeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.LinkedHashSet;

/**
 * ProductTypeDAO implementation
 */
@Transactional
@Repository("productTypeDAO")
public class ProductTypeDAOImpl implements ProductTypeDAO {

    private static final Logger log = Logger.getLogger(ProductTypeDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public ProductType findProductType(String typeName){
        log.debug("Get subcategory with name: " + typeName);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findProductType").setString("typeName", typeName);
        return (ProductType) query.uniqueResult();
    }

    @Override
    public LinkedHashSet<ProductType> findAll() {
        log.debug("Get all subcategories");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAllProductType");
        LinkedHashSet<ProductType> productTypes = new LinkedHashSet<ProductType>((query.list()));
        return productTypes;
    }

    @Override
    public Category getCategory(String typeName) {
        log.debug("Get category of subcategory: " + typeName);
        ProductType productType = findProductType(typeName);
        Category category = productType.getCategory();
        return category;
    }

    @Override
    public LinkedHashSet<Attribute> getCategoryAttributes(String typeName) {
        log.debug("Get attributes of subcategory: " + typeName);
        ProductType productType = findProductType(typeName);
        LinkedHashSet<Attribute> attributes = new LinkedHashSet<Attribute>(productType.getAttributes());
        return attributes;
    }

    @Override
    public void saveProductType(ProductTypeInfo productTypeInfo){
        log.info("Save new subcategory: " + productTypeInfo.getTypeName());
        ProductType productType = new ProductType();
        productType.setTypeName(productTypeInfo.getTypeName());
        productType.setAttributes(productTypeInfo.getAttributes());
        productType.setCategory(categoryDAO.findCategory(productTypeInfo.getCategory()));
        sessionFactory.getCurrentSession().persist(productType);
    }
}
