package org.o7planning.springmvcshoppingcart.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.o7planning.springmvcshoppingcart.dao.ProductDAO;
import org.o7planning.springmvcshoppingcart.dao.ProductTypeDAO;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.model.BannerProduct;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ProductDAO implementation
 */
// Transactional for Hibernate
@Transactional
@Repository("productDAO")
public class ProductDAOImpl implements ProductDAO {

    private static final Logger log = Logger.getLogger(ProductDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ProductTypeDAO productTypeDAO;

    @Override
    public Product findProduct(String code) {
        log.debug("Find product with code: " + code);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findProduct").setString("code", code);
        return (Product) query.uniqueResult();
    }

    @Override
    public ProductInfo findProductInfo(String code) {
        Product product = findProduct(code);
        if (product == null) {
            return null;
        }
        return new ProductInfo(product.getCode(), product.getName(), product.getPrice(), product.getProductType(), product.getStorage(), product.isBanner());
    }

    @Override
    public List<BannerProduct> getBannerParoducts() {
        log.debug("Get products for banner");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("bannerProducts");
        List<BannerProduct> bannerProducts = new ArrayList<>(query.list());
        return bannerProducts;
    }

    @Override
    public List<ProductInfo> getProductsBySubcategory(String productType) {
        log.debug("Find products with subcategory: " + productType);
        Session session = sessionFactory.getCurrentSession();
        org.hibernate.query.Query query = session.getNamedQuery("productBySubcategory").setString("productType", productTypeDAO.findProductType(productType).getId().toString());

        List<ProductInfo> products = new ArrayList<ProductInfo>((query.list()));
        return products;
    }

    @Override
    public void save(ProductInfo productInfo) {
        String code = productInfo.getCode();
        Product product = null;

        boolean isNew = false;
        if (code != null) {
            log.debug("Update product with code: " + code);
            product = this.findProduct(code);
        }
            isNew = true;
            if (product == null) {
            log.info("Save new product");
            product = new Product();
            product.setCreateDate(new Date());
        }
        product.setCode(code);
        product.setName(productInfo.getName());
        product.setPrice(productInfo.getPrice());
        product.setProductType(productTypeDAO.findProductType(productInfo.getProductType()) );
        product.setStorage(productInfo.getStorage());
        product.setBanner(productInfo.isBanner());
        if (productInfo.getFileData() != null) {
            byte[] image = productInfo.getFileData().getBytes();
            if (image != null && image.length > 0) {
                product.setImage(image);
            }
        }
        if (isNew) {
            this.sessionFactory.getCurrentSession().persist(product);
        }

        this.sessionFactory.getCurrentSession().flush();
    }

    @Override
    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage,
                                                       String likeName) {
        log.debug("Get product query");
        Session session = sessionFactory.getCurrentSession();
        Query query;
        if (likeName != null && likeName.length() > 0) {
           query = session.getNamedQuery("findProductsWithName").setString("likeName", likeName);
        } else {
            query = session.getNamedQuery("findProducts");
        }

        return new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
        return queryProducts(page, maxResult, maxNavigationPage, null);
    }

}
