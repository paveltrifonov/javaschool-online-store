package org.o7planning.springmvcshoppingcart.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.o7planning.springmvcshoppingcart.dao.AttributeDAO;
import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.model.AttributeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * AttributeDAO implementation
 */
@Transactional
@Repository("attributeDAO")
public class AttributeDAOImpl implements AttributeDAO {

    private static final Logger log = Logger.getLogger(AttributeDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Attribute findAttribute(String attributeName) {
        log.debug("Find attribute by name: " + attributeName);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAttribute").setString("attributeName", attributeName);
        return (Attribute) query.uniqueResult();
    }

    @Override
    public LinkedHashSet<Attribute> findAll() {
        log.debug("Get all attributes");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("findAllAttribute");
        LinkedHashSet<Attribute> attributes = new LinkedHashSet<Attribute>(query.list());
        return attributes;
    }

    @Override
    public List<AttributeValue> findValues(String attributeName) {
        log.debug("Find values of attribute with name: " + attributeName);
        Attribute attribute =  findAttribute(attributeName);
        List<AttributeValue> attributeValues = new ArrayList<AttributeValue>(attribute.getAttributeValues());
        return attributeValues;
    }

    @Override
    public void saveAttribute(AttributeInfo attributeInfo) {
        log.info("Save new attribute: " + attributeInfo.getAttributeName());
        Attribute attribute = new Attribute();
        attribute.setAttributeName(attributeInfo.getAttributeName());
        sessionFactory.getCurrentSession().persist(attribute);

    }
}
