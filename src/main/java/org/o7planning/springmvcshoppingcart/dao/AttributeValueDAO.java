package org.o7planning.springmvcshoppingcart.dao;

import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.model.AttributeValueInfo;

import java.util.LinkedHashSet;

/**
 * Parameters value dao
 */

public interface AttributeValueDAO {
    /**
     * Get parameter value by id
     * @param id the id of parameter value
     * @return
     */
    AttributeValue findAttributeValue(Integer id);

    /**
     * Get all values
     * @return set of values
     */
    LinkedHashSet<AttributeValue> findAll();

    /**
     * Save new parameter value
     */
    void saveAttributeValue(AttributeValueInfo attributeValueInfo);
}
