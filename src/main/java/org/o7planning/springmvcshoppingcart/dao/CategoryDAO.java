package org.o7planning.springmvcshoppingcart.dao;

import org.o7planning.springmvcshoppingcart.entity.Category;
import org.o7planning.springmvcshoppingcart.entity.ProductType;

import java.util.LinkedHashSet;

/**
 * Category dao
 */

public interface CategoryDAO {
    /**
     * Get category by name
     * @param categoryName category name
     * @return category
     */
    Category findCategory(String categoryName);

    /**
     * Get all categories
     */
    LinkedHashSet<Category> findAll();

    /**
     * Get subcategories of category
     * @param categoryName category name
     * @return set of subcategories
     */
    LinkedHashSet<ProductType> getProductTypes(String categoryName);

    /**
     * Save new category
     */
    void saveCategory(Category category);

}
