package org.o7planning.springmvcshoppingcart.dao;

import org.o7planning.springmvcshoppingcart.entity.Attribute;
import org.o7planning.springmvcshoppingcart.entity.AttributeValue;
import org.o7planning.springmvcshoppingcart.model.AttributeInfo;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Product parameters dao
 */

public interface AttributeDAO {
    /**
     * Find parameter by name
     * @param attributeName parameter name
     * @return attribute
     */

    Attribute findAttribute(String attributeName);

    /**
     * Get all parameters
     * @return  set of parameters
     */
    LinkedHashSet<Attribute> findAll();

    /**
     * Save new parameter
     * @param attributeInfo information to save new parameter
     */
    void saveAttribute(AttributeInfo attributeInfo);

    /** Get values of parameter
     * @param attributeName parameter name
     * @return list of values of parameter
     */
    List<AttributeValue> findValues(String attributeName);

}
