package org.o7planning.springmvcshoppingcart.dao;

import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.model.BannerProduct;
import org.o7planning.springmvcshoppingcart.model.PaginationResult;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;

import java.util.List;

/**
 * Product dao
 */
public interface ProductDAO {

    /**
     * Get product by code
     * @param code code of product
     * @return product
     */
    Product findProduct(String code);

    /**
     * Get product in ProductInfo
     * @param code product code
     */
    ProductInfo findProductInfo(String code) ;

    /**
     * Get list of products by category
     * @param productType subcategory
     * @return list of products
     */
    List<ProductInfo> getProductsBySubcategory(String productType);

    /**
     * Get list of products for banner
     */
    List<BannerProduct> getBannerParoducts();

    /**
     * Page list of products
     */
    PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage  );

    /**
     * Page list of products
     * @param likeName name of product
     * @return
     */
    PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName);

    /**
     * Save or update product
     */
    void save(ProductInfo productInfo);

}