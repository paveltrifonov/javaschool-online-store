package org.o7planning.springmvcshoppingcart.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.o7planning.springmvcshoppingcart.dao.OrderDAO;
import org.o7planning.springmvcshoppingcart.exceptions.OrderNotFoundException;
import org.o7planning.springmvcshoppingcart.model.OrderInfo;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrderServiceImplTest {

    @Mock
    private OrderDAO orderDAO;

    @InjectMocks
    private OrderService orderService = new OrderServiceImpl();

    private String id;
    OrderInfo orderInfo = new OrderInfo();

    @Before
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
        id ="1";
        orderInfo.setId(id);
        orderInfo.setOrderNum(1);
        orderInfo.setOrderDate(new Date());
        orderInfo.setAmount(new BigDecimal(10));
    }

    @Test
    public void orderFindTest(){
        when(orderDAO.findOrder(id)).thenReturn(null);
        orderService.findOrder(id);
    }

    @Test
    public void updateStatusTest() {

        orderService.updateStatus(id, "Sent");
        verify(orderDAO).updateStatus(id, "Sent");
    }

    @Test
    public void getOrderInfoTest() throws OrderNotFoundException {
        when(orderDAO.getOrderInfo(id)).thenReturn(orderInfo);
        Assert.assertEquals(orderInfo, orderService.getOrderInfo(id));
    }
}