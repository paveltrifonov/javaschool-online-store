package org.o7planning.springmvcshoppingcart.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.o7planning.springmvcshoppingcart.dao.AccountDAO;
import org.o7planning.springmvcshoppingcart.dao.ProductDAO;
import org.o7planning.springmvcshoppingcart.entity.Account;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class AccountServiceImplTest {

    @Mock
    private AccountDAO accountDAO;

    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    private Account account;
    private String userName;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userName = "Ivan";
        account = new Account();
        account.setUserName(userName);
    }

    @Test
    public void findAccountTest() {

        when(accountDAO.findAccount(userName)).thenReturn(account);
        accountService.findAccount(userName);
    }
    @Test
    public void isUserNameUniqueTest() {
        when(accountDAO.findAccount(userName)).thenReturn(account);
        Assert.assertEquals(true, accountService.isUserNameUnique(userName));
    }
}