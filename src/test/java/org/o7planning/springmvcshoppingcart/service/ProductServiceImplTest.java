package org.o7planning.springmvcshoppingcart.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.o7planning.springmvcshoppingcart.dao.ProductDAO;
import org.o7planning.springmvcshoppingcart.entity.Product;
import org.o7planning.springmvcshoppingcart.exceptions.NotEnoughProductsException;
import org.o7planning.springmvcshoppingcart.exceptions.ProductNotFoundException;
import org.o7planning.springmvcshoppingcart.model.BannerProduct;
import org.o7planning.springmvcshoppingcart.model.CartLineInfo;
import org.o7planning.springmvcshoppingcart.model.ProductInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ProductServiceImplTest {

    @Mock
    private ProductDAO productDAO;

    @InjectMocks
    private ProductService productService = new ProductServiceImpl();

    private Product product;
    private ProductInfo productInfo;
    private String code;
    private List<CartLineInfo> cartInfo = new ArrayList<>();
    private CartLineInfo cartLineInfo = new CartLineInfo();
    private List<BannerProduct> bannerList = new ArrayList<>();

    @Before
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
        code = "1";
        product = new Product();
        product.setCode(code);
        product.setName("Ivan");
        product.setCreateDate(new Date());
        product.setStorage(1);
        productInfo = new ProductInfo();
        productInfo.setCode(code);
        productInfo.setName("Ivan");
        productInfo.setStorage(1);
    }

    @Test
    public void findProductTest() throws ProductNotFoundException {

        when(productDAO.findProduct(code)).thenReturn(product);
        Product retrivedProduct = productService.findProduct(code);
        Assert.assertEquals(product, retrivedProduct);

    }

    @Test(expected = ProductNotFoundException.class)
    public void findProductTest_Not_Found() throws ProductNotFoundException {

        when(productDAO.findProduct(code)).thenReturn(null);
        productService.findProduct(code);

    }

    @Test
    public void getBannerProductsTest(){

        when(productDAO.getBannerParoducts()).thenReturn(bannerList);
        productService.getBannerProducts();
    }

    @Test
    public void checkCartTest()  throws ProductNotFoundException, NotEnoughProductsException{

        cartLineInfo.setQuantity(1);
        cartLineInfo.setProductInfo(productInfo);
        cartInfo.add(cartLineInfo);
        when(productDAO.findProduct(code)).thenReturn(product);
        productService.checkCart(cartInfo);
        verify(productDAO).save(cartLineInfo.getProductInfo());
    }

    @Test(expected = NotEnoughProductsException.class)
    public void checkCartTest_Not_Enough_Products() throws ProductNotFoundException, NotEnoughProductsException{

        cartLineInfo.setQuantity(2);
        cartLineInfo.setProductInfo(productInfo);
        cartInfo.add(cartLineInfo);
        when(productDAO.findProduct(code)).thenReturn(product);
        productService.checkCart(cartInfo);

    }
}